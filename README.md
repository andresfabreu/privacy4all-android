# Privacy4All

* Login com facebook não vai funcionar devido a estar como modo desenvolvimento, somente pessoas autorizadas conseguem realizar o login. 
* Login deve ser realizado via modo anônimo

## Arquitetura: Clean
### Camada Data

* Separado por origem de dados, no caso do projeto: 
* 1- in_memory (memória em tempo de execução)
* 2- network (camada de comunicação REST)
* 3- preferences (sharedPreferences cache)

* Cada origem de dados possue: 
* 1- data_source (origem dos dados)
* 2- model (modelo dos dados do data_source)
* 3- mapper (mapeamento do modelo do data_source para o modelo de domain)

* A comunicação com a camada de data e a lógica de como os dados vão ser obtidos (via cache, memória, REST) estão presentes nos repositórios (package repository)

### Camada Domain

* Camada que contém as regras de negócio da aplicação (useCases, models, exceções, interface implementada pelo repositório para ser utilizada nos useCases)

### Camada Presentation

* Camada responsável pela interface do aplicativo e a exibição dos dados recebidos do domínio
* Separação feita por scenes, onde cada scene representa no caso da aplicação uma tela, cada scene irá ter (viewModel, mapper, presenter, componente/módulo de injeção de dependência)

### A comunicação entre as camadas é feita de forma reativa (utlizando RX), também é utilizado dagger para injeção de dependências

## Libs utilizadas

1. Espresso para realização de testes de integração
2. Mockito para realização de testes unitários
3. Retrofit para realização de chamadas REST
4. Dagger para realização de injeção de dependência
5. RXKotlin/RxAndroid para utilização de programação reativa (RX)
6. RXBinding para implementação de eventos de clicks de forma reativa

## Execução do Projeto

* Utilizar a build variant "Debug" e executar normalmente

## Execução de Teste

* Clicar com o botão direito no packages (androidTest ou test) e selecionar "Run 'Tests in....'"
