package com.privacy4all

import android.content.Context
import android.os.SystemClock
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.privacy4all.R.id.*
import com.privacy4all.presentation.scene.category_protocol.CategoryProtocolAdapter
import com.privacy4all.presentation.scene.home.HomeAdapter
import com.privacy4all.presentation.scene.login.LoginActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File

@RunWith(AndroidJUnit4::class)
class AppFlowTest {

    @Rule
    @JvmField
    var activityRule: ActivityTestRule<LoginActivity> = ActivityTestRule<LoginActivity>(LoginActivity::class.java, false, false)

    @Before
    fun setup() {
        val root = InstrumentationRegistry.getTargetContext().filesDir.parentFile
        val sharedPreferencesFileNames = File(root, "shared_prefs").list()
        for (fileName in sharedPreferencesFileNames) {
            InstrumentationRegistry.getTargetContext().getSharedPreferences(fileName.replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit()
        }
    }

    @Test
    fun testAppFlow() {
        // Context of the app under test.
        activityRule.launchActivity(null)

        onView(withId(loginWithUuidButton)).check(matches(isCompletelyDisplayed()))
        onView(withId(loginWithUuidButton)).perform(click())
        SystemClock.sleep(1000)
        onView(withId(continueButton)).check(matches(isCompletelyDisplayed()))
        onView(withId(continueButton)).perform(click())
        SystemClock.sleep(1000)
        onView(withId(categoryRecycler)).check(matches(isCompletelyDisplayed()))
        onView(ViewMatchers.withId(categoryRecycler)).perform(RecyclerViewActions.actionOnItemAtPosition<HomeAdapter.CategoryViewHolder>(0, click()))
        SystemClock.sleep(1000)
        onView(withId(questionRecycler)).check(matches(isDisplayed()))
        onView(ViewMatchers.withId(questionRecycler)).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryProtocolAdapter.AnswerViewHolder>(2, click()))
        SystemClock.sleep(1000)
        onView(withId(nextButton)).perform(click())
        onView(ViewMatchers.withId(questionRecycler)).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryProtocolAdapter.AnswerViewHolder>(2, click()))
        SystemClock.sleep(1000)
        onView(withId(nextButton)).perform(click())
        onView(ViewMatchers.withId(questionRecycler)).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryProtocolAdapter.AnswerViewHolder>(2, click()))
        SystemClock.sleep(1000)
        onView(withId(doneButton)).perform(click())
        SystemClock.sleep(1000)
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext())
        SystemClock.sleep(1000)
        onView(withText("Finalizar sessão")).perform(click())
        SystemClock.sleep(1000)
        onView(withId(loginWithUuidButton)).check(matches(isCompletelyDisplayed()))
    }
}
