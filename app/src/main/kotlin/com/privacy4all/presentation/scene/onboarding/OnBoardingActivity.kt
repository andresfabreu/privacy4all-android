package com.privacy4all.presentation.scene.onboarding

import android.os.Bundle
import com.privacy4all.R
import com.privacy4all.presentation.common.ui.BaseActivity

class OnBoardingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onbording)
        setTransparentStatusBar()
        displayFragment(OnBoardingFragment())
    }

}