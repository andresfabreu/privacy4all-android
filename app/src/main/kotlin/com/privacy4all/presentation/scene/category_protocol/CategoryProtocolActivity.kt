package com.privacy4all.presentation.scene.category_protocol

import android.os.Bundle
import com.privacy4all.R
import com.privacy4all.presentation.common.ui.BaseActivity

class CategoryProtocolActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_protocol)
        displayFragment(CategoryProtocolFragment())
        setTransparentStatusBar()
    }

}

