package com.privacy4all.presentation.scene.onboarding

import android.content.Context
import com.privacy4all.R
import com.privacy4all.domain.model.User
import com.privacy4all.presentation.common.dependency_injection.qualifier.ActivityContext
import javax.inject.Inject

class OnBoardingViewModelMapper @Inject constructor(@ActivityContext val context: Context) {
    fun map(user: User): UserViewModel {
        with(user) {
            return UserViewModel(String.format("%s %s", context.getString(R.string.welcome), name), pictureUrl)
        }
    }
}