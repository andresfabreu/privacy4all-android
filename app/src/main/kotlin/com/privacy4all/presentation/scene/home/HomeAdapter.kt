package com.privacy4all.presentation.scene.home

import android.graphics.Color
import android.graphics.drawable.LayerDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.privacy4all.R
import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.utils.loadUrlWithPlaceholder
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_category.*

class HomeAdapter(private var categoryViewModelList: List<CategoryViewModel>) : RecyclerView.Adapter<HomeAdapter.CategoryViewHolder>() {

    private val onItemClickSubject: PublishSubject<CategoryViewModel> = PublishSubject.create()

    override fun onBindViewHolder(holder: CategoryViewHolder?, position: Int) {
        holder?.bind(categoryViewModelList[position], categoryViewModelList.size - 1 == position, onItemClickSubject)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_category, parent, false))
    }

    override fun getItemCount(): Int {
        return categoryViewModelList.size
    }

    fun getOnItemClickObservable(): Observable<CategoryViewModel> {
        return onItemClickSubject
    }

    fun updateViewModel(categoryViewModelList: List<CategoryViewModel>) {
        this.categoryViewModelList = categoryViewModelList
        notifyDataSetChanged()
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView = itemView

        fun bind(categoryViewModel: CategoryViewModel, isLast: Boolean, onItemClickSubject: PublishSubject<CategoryViewModel>) {
            with(categoryViewModel) {
                val categoryColor = Color.parseColor(colorHex)
                val transparentColor = ContextCompat.getColor(containerView.context, R.color.bon_jour)
                categoryName.text = name
                categoryName.setTextColor(categoryColor)
                categoryDescription.text = description
                categoryImage.loadUrlWithPlaceholder(iconUrl, R.drawable.ic_placeholder_categorias)
                (categoryProgress.progressDrawable as LayerDrawable).findDrawableByLayerId(R.id.progressBackground).mutate().setColorFilter(if (progress > 0) categoryColor else transparentColor, android.graphics.PorterDuff.Mode.SRC_ATOP)
                categoryProgress.progress = progress
                RxView.clicks(categoryLayout).map { this }.subscribe(onItemClickSubject)
            }
            containerView.setPadding(0,
                    if (adapterPosition == 0) containerView.context.resources.getDimensionPixelOffset(R.dimen.category_padding) else 0,
                    0,
                    if (isLast) containerView.context.resources.getDimensionPixelOffset(R.dimen.category_padding) else 0)
        }
    }
}