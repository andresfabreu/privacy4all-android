package com.privacy4all.presentation.scene.category_protocol

data class QuestionViewModel(var id: Int = 0,
                             var text: String = "",
                             var isMultipleChoice: Boolean = false,
                             var answers: List<AnswerViewModel> = ArrayList())

data class AnswerViewModel(var id: Int = 0,
                           var text: String = "",
                           var isSelected: Boolean = false)

class GetQuestionsRequest(val protocolId: Int = 0,
                          val categoryId: Int = 0)

class SendAnswerRequest(val protocolId: Int = 0,
                        val categoryId: Int = 0,
                        val questionId: Int = 0,
                        val id: Int = 0)