package com.privacy4all.presentation.scene.login

import com.privacy4all.presentation.common.ui.BaseView

interface LoginView : BaseView {
    fun navigateToOnBoarding()
    fun navigateToHome()
    fun displayLoginButton()
}