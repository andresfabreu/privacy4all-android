package com.privacy4all.presentation.scene.category_protocol

import com.privacy4all.domain.use_case.GetQuestionsUseCase
import com.privacy4all.domain.use_case.GetSelectedCategoryUseCase
import com.privacy4all.domain.use_case.SendAnswerUseCase
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import com.privacy4all.presentation.common.mapper.CategoryViewModelMapper
import com.privacy4all.presentation.common.mapper.ErrorViewModelMapper
import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.model.ErrorAction
import com.privacy4all.presentation.common.ui.BasePresenter
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@PerScene
class CategoryProtocolPresenter @Inject constructor(private val categoryProtocolView: CategoryProtocolView,
                                                    private val getSelectedCategoryUseCase: GetSelectedCategoryUseCase,
                                                    private val getQuestionsUseCase: GetQuestionsUseCase,
                                                    private val sendAnswerUseCase: SendAnswerUseCase,
                                                    private val categoryViewModelMapper: CategoryViewModelMapper,
                                                    private val questionViewModelMapper: QuestionViewModelMapper,
                                                    private val answerViewModelMapper: AnswerViewModelMapper,
                                                    private val errorViewModelMapper: ErrorViewModelMapper) : BasePresenter() {

    private var questionViewModelList: List<QuestionViewModel> = ArrayList()
    private var currentQuestion: Int = 0

    fun getSelectedCategory() {
        getSelectedCategoryUseCase.getSingle()
                .map { categoryViewModelMapper.map(it) }
                .subscribeWith(object : DisposableSingleObserver<CategoryViewModel>() {
                    override fun onError(e: Throwable) {
                        categoryProtocolView.displayError(errorViewModelMapper.map(e, ErrorAction.DISPLAY_DIALOG_AND_NAVIGATE_BACK))
                    }

                    override fun onSuccess(categoryViewModel: CategoryViewModel) {
                        categoryProtocolView.displayCategory(categoryViewModel)
                    }

                }).addTo(compositeDisposable)
    }

    fun getQuestions(request: GetQuestionsRequest) {
        categoryProtocolView.displayLoading()
        getQuestionsUseCase.getSingle(questionViewModelMapper.map(request))
                .map { questionViewModelMapper.map(it) }
                .subscribeWith(object : DisposableSingleObserver<List<QuestionViewModel>>() {
                    override fun onSuccess(viewModelList: List<QuestionViewModel>) {
                        questionViewModelList = viewModelList
                        categoryProtocolView.dismissLoading()
                        categoryProtocolView.displayQuestion(questionViewModelList[currentQuestion], currentQuestion, questionViewModelList.size)
                        categoryProtocolView.hidePrevButton()
                    }

                    override fun onError(e: Throwable) {
                        categoryProtocolView.displayError(errorViewModelMapper.map(e, ErrorAction.CUSTOM_ERROR_HANDLE))
                    }

                }).addTo(compositeDisposable)
    }

    fun sendAnswer(request: SendAnswerRequest) {
        categoryProtocolView.displayLoading()
        sendAnswerUseCase.getSingle(answerViewModelMapper.map(request))
                .map { questionViewModelMapper.map(it) }
                .subscribeWith(object : DisposableSingleObserver<List<QuestionViewModel>>() {
                    override fun onSuccess(viewModelList: List<QuestionViewModel>) {
                        questionViewModelList = viewModelList
                        categoryProtocolView.dismissLoading()
                        categoryProtocolView.displayQuestion(questionViewModelList[currentQuestion], currentQuestion, questionViewModelList.size)
                    }

                    override fun onError(e: Throwable) {
                        categoryProtocolView.displayError(errorViewModelMapper.map(e, ErrorAction.DISPLAY_DIALOG))
                    }

                }).addTo(compositeDisposable)
    }

    fun getNextQuestion() {
        if (currentQuestion < questionViewModelList.size - 1) {
            currentQuestion++
            categoryProtocolView.updateQuestion(questionViewModelList[currentQuestion], currentQuestion)
            if (currentQuestion == questionViewModelList.size - 1) {
                categoryProtocolView.hideNextButton()
            } else {
                categoryProtocolView.showNextButton()
            }

            categoryProtocolView.showPrevButton()
        }
    }

    fun getPrevQuestion() {
        if (currentQuestion > 0) {
            currentQuestion--
            categoryProtocolView.updateQuestion(questionViewModelList[currentQuestion], currentQuestion)
            if (currentQuestion == 0) {
                categoryProtocolView.hidePrevButton()
            } else {
                categoryProtocolView.showPrevButton()
            }

            categoryProtocolView.showNextButton()
        }
    }

}