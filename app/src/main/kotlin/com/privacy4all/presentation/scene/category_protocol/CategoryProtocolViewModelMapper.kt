package com.privacy4all.presentation.scene.category_protocol

import com.privacy4all.domain.model.Answer
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import com.privacy4all.domain.use_case.SendAnswerUseCase
import com.privacy4all.presentation.common.mapper.ViewModelMapper
import javax.inject.Inject

class QuestionViewModelMapper @Inject constructor(private val answerViewModelMapper: AnswerViewModelMapper) : ViewModelMapper<Question, QuestionViewModel>() {
    override fun map(model: Question): QuestionViewModel {
        with(model) {
            return QuestionViewModel(id, text, isMultipleChoice, answerViewModelMapper.map(answers))
        }
    }

    fun map(request: GetQuestionsRequest): GetQuestionsUseCase.Request {
        with(request) {
            return GetQuestionsUseCase.Request(protocolId, categoryId)
        }
    }
}

class AnswerViewModelMapper @Inject constructor() : ViewModelMapper<Answer, AnswerViewModel>() {
    override fun map(model: Answer): AnswerViewModel {
        with(model) {
            return AnswerViewModel(id, text, isSelected)
        }
    }

    fun map(request: SendAnswerRequest): SendAnswerUseCase.Request {
        with(request) {
            return SendAnswerUseCase.Request(protocolId, categoryId, questionId, id)
        }
    }

}
