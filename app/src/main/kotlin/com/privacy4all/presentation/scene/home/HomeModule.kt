package com.privacy4all.presentation.scene.home

import android.content.Context
import com.privacy4all.data.network.data_source.AuthApiDataSource
import com.privacy4all.data.network.data_source.CategoryApiDataSource
import com.privacy4all.data.network.data_source_factory.AuthApiDataSourceFactory
import com.privacy4all.data.network.data_source_factory.CategoryApiDataSourceFactory
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import com.privacy4all.data.repository.AuthRepository
import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.repository_data.AuthDataRepository
import com.privacy4all.domain.repository_data.CategoryDataRepository
import com.privacy4all.domain.repository_data.UserDataRepository
import com.privacy4all.presentation.common.dependency_injection.qualifier.ActivityContext
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import dagger.Module
import dagger.Provides

@Module
class HomeModule(private val homeView: HomeView, private val context: Context) {

    @Provides
    @ActivityContext
    @PerScene
    fun provideActivityContext(): Context {
        return context
    }

    @Provides
    @PerScene
    internal fun provideHomeView(): HomeView {
        return homeView
    }

    @Provides
    @PerScene
    fun provideAuthApiDataSource(sessionPreferencesDataSource: SessionPreferencesDataSource): AuthApiDataSource {
        return AuthApiDataSourceFactory(sessionPreferencesDataSource).authApiDataSource
    }

    @Provides
    @PerScene
    fun provideCategoryApiDataSource(sessionPreferencesDataSource: SessionPreferencesDataSource): CategoryApiDataSource {
        return CategoryApiDataSourceFactory(sessionPreferencesDataSource).categoryApiDataSource
    }

    @Provides
    @PerScene
    fun provideAuthRepository(authRepository: AuthRepository): AuthDataRepository {
        return authRepository
    }

    @Provides
    @PerScene
    fun provideUserRepository(userRepository: UserRepository): UserDataRepository {
        return userRepository
    }

    @Provides
    @PerScene
    fun provideCategoryRepository(categoryRepository: CategoryRepository): CategoryDataRepository {
        return categoryRepository
    }
}