package com.privacy4all.presentation.scene.onboarding

import com.privacy4all.presentation.common.ui.BaseView

interface OnBoardingView : BaseView {
    fun displayUser(userViewModel: UserViewModel)
}