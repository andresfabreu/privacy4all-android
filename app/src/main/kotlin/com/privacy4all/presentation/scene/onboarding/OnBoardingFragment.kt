package com.privacy4all.presentation.scene.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.privacy4all.R
import com.privacy4all.presentation.common.AndroidApplication
import com.privacy4all.presentation.common.Navigator
import com.privacy4all.presentation.common.ui.BaseFragment
import com.privacy4all.presentation.common.utils.asBold
import com.privacy4all.presentation.common.utils.loadUrlAsCircle
import kotlinx.android.synthetic.main.fragment_onboarding.*
import javax.inject.Inject


class OnBoardingFragment : BaseFragment(), OnBoardingView {

    @Inject
    lateinit var onBoardingPresenter: OnBoardingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerOnBoardingComponent.builder()
                .applicationComponent(AndroidApplication.component)
                .onBoardingModule(OnBoardingModule(this, context!!))
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_onboarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onBoardingPresenter.setOnBoardingAsDone()
        onBoardingPresenter.getUserInfo()
        descriptionText.text = asBold(descriptionText.text.toString(), 40, 61)
        RxView.clicks(continueButton).subscribe { Navigator.openHome(context) }
    }

    override fun onDestroyView() {
        onBoardingPresenter.destroy()
        super.onDestroyView()
    }

    override fun displayUser(userViewModel: UserViewModel) {
        with(userViewModel) {
            userImage.loadUrlAsCircle(pictureUrl)
            welcomeText.text = asBold(name, 9, name.length)
        }
    }
}