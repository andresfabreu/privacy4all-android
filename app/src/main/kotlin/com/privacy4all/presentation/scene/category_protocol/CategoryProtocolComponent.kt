package com.privacy4all.presentation.scene.category_protocol

import com.privacy4all.presentation.common.dependency_injection.component.ApplicationComponent
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import dagger.Component

@PerScene
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(CategoryProtocolModule::class))
interface CategoryProtocolComponent {
    fun inject(categoryProtocolFragment: CategoryProtocolFragment)
}