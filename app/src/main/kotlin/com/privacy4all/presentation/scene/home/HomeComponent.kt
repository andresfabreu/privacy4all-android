package com.privacy4all.presentation.scene.home

import com.privacy4all.presentation.common.dependency_injection.component.ApplicationComponent
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import dagger.Component

@PerScene
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(HomeModule::class))
interface HomeComponent {
    fun inject(homeFragment: HomeFragment)
}