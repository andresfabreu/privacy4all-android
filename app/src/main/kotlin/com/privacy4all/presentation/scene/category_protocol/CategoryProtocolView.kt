package com.privacy4all.presentation.scene.category_protocol

import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.ui.BaseView

interface CategoryProtocolView : BaseView {
    fun displayCategory(categoryViewModel: CategoryViewModel)
    fun displayQuestion(questionViewModel: QuestionViewModel, currentQuestion: Int, totalQuestions: Int)
    fun updateQuestion(questionViewModel: QuestionViewModel, currentQuestion: Int)
    fun hidePrevButton()
    fun hideNextButton()
    fun showPrevButton()
    fun showNextButton()
}