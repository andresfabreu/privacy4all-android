package com.privacy4all.presentation.scene.onboarding

import android.content.Context
import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.repository_data.UserDataRepository
import com.privacy4all.presentation.common.dependency_injection.qualifier.ActivityContext
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import dagger.Module
import dagger.Provides

@Module
class OnBoardingModule(private val onBoardingView: OnBoardingView, private val context: Context) {

    @Provides
    @ActivityContext
    @PerScene
    fun provideActivityContext(): Context {
        return context
    }

    @Provides
    @PerScene
    internal fun provideOnBoardingView(): OnBoardingView {
        return onBoardingView
    }

    @Provides
    @PerScene
    fun provideUserRepository(userRepository: UserRepository): UserDataRepository {
        return userRepository
    }
}