package com.privacy4all.presentation.scene.login

import com.privacy4all.domain.use_case.IsOnBoardingDoneUseCase
import com.privacy4all.domain.use_case.IsUserLoggedInUseCase
import com.privacy4all.domain.use_case.LoginWithFacebookUseCase
import com.privacy4all.domain.use_case.LoginWithUuidUseCase
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import com.privacy4all.presentation.common.mapper.ErrorViewModelMapper
import com.privacy4all.presentation.common.model.ErrorAction
import com.privacy4all.presentation.common.ui.BasePresenter
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@PerScene
class LoginPresenter @Inject constructor(private val loginWithFacebookUseCase: LoginWithFacebookUseCase,
                                         private val loginWithUuidUseCase: LoginWithUuidUseCase,
                                         private val isUserLoggedInUseCase: IsUserLoggedInUseCase,
                                         private val isOnBoardingDoneUseCase: IsOnBoardingDoneUseCase,
                                         private val errorViewModelMapper: ErrorViewModelMapper,
                                         private val loginView: LoginView) : BasePresenter() {

    fun isUserLoggedIn() {
        isUserLoggedInUseCase.getSingle().subscribeWith(object : DisposableSingleObserver<Boolean>() {
            override fun onError(e: Throwable) {
                loginView.displayLoginButton()
            }

            override fun onSuccess(isUserLoggedIn: Boolean) {
                if (isUserLoggedIn) {
                    loginView.navigateToHome()
                } else {
                    loginView.displayLoginButton()
                }
            }
        }).addTo(compositeDisposable)
    }

    fun loginWithFacebook() {
        loginView.displayLoading()
        Single.zip(loginWithFacebookUseCase.getSingle(),
                isOnBoardingDoneUseCase.getSingle(),
                BiFunction<Unit, Boolean, Boolean> { _, isOnBoardingDone -> isOnBoardingDone })
                .subscribeWith(object : DisposableSingleObserver<Boolean>() {
                    override fun onError(e: Throwable) {
                        loginView.displayError(errorViewModelMapper.map(e, ErrorAction.DISPLAY_DIALOG))
                    }

                    override fun onSuccess(isOnBoardingDone: Boolean) {
                        loginView.dismissLoading()
                        if (isOnBoardingDone) {
                            loginView.navigateToHome()
                        } else {
                            loginView.navigateToOnBoarding()
                        }
                    }
                }).addTo(compositeDisposable)
    }

    fun loginWithUuid() {
        loginView.displayLoading()
        Single.zip(loginWithUuidUseCase.getSingle(),
                isOnBoardingDoneUseCase.getSingle(),
                BiFunction<Unit, Boolean, Boolean> { _, isOnBoardingDone -> isOnBoardingDone })
                .subscribeWith(object : DisposableSingleObserver<Boolean>() {
                    override fun onError(e: Throwable) {
                        loginView.displayError(errorViewModelMapper.map(e, ErrorAction.DISPLAY_DIALOG))
                    }

                    override fun onSuccess(isOnBoardingDone: Boolean) {
                        loginView.dismissLoading()
                        if (isOnBoardingDone) {
                            loginView.navigateToHome()
                        } else {
                            loginView.navigateToOnBoarding()
                        }
                    }
                }).addTo(compositeDisposable)
    }
}