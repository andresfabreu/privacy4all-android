package com.privacy4all.presentation.scene.login

import com.privacy4all.presentation.common.dependency_injection.component.ApplicationComponent
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import dagger.Component

@PerScene
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(LoginModule::class))
interface LoginComponent {
    fun inject(loginFragment: LoginFragment)
}