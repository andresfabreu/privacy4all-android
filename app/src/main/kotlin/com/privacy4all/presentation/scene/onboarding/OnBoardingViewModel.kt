package com.privacy4all.presentation.scene.onboarding

data class UserViewModel(var name: String = "",
                         var pictureUrl: String = "")
