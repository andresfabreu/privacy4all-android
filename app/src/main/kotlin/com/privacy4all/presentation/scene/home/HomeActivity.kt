package com.privacy4all.presentation.scene.home

import android.os.Bundle
import com.privacy4all.R
import com.privacy4all.presentation.common.ui.BaseActivity

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        displayFragment(HomeFragment())
    }

}