package com.privacy4all.presentation.scene.home

import com.privacy4all.domain.use_case.*
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import com.privacy4all.presentation.common.mapper.CategoryViewModelMapper
import com.privacy4all.presentation.common.mapper.ErrorViewModelMapper
import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.model.ErrorAction
import com.privacy4all.presentation.common.model.GetCategoriesRequest
import com.privacy4all.presentation.common.ui.BasePresenter
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@PerScene
class HomePresenter @Inject constructor(private val homeView: HomeView,
                                        private val getUserInfoUseCase: GetUserInfoUseCase,
                                        private val logoutUseCase: LogoutUseCase,
                                        private val getCategoriesUseCase: GetCategoriesUseCase,
                                        private val setSelectedCategoryUseCase: SetSelectedCategoryUseCase,
                                        private val shouldReloadCategoriesUseCase: ShouldReloadCategoriesUseCase,
                                        private val categoryViewModelMapper: CategoryViewModelMapper,
                                        private val errorViewModelMapper: ErrorViewModelMapper) : BasePresenter() {

    fun getUserPicture() {
        getUserInfoUseCase.getSingle()
                .map { it.pictureUrl }
                .subscribeWith(object : DisposableSingleObserver<String>() {
                    override fun onError(e: Throwable) {

                    }

                    override fun onSuccess(pictureUrl: String) {
                        homeView.displayUserPicture(pictureUrl)
                    }

                }).addTo(compositeDisposable)
    }

    fun logout() {
        homeView.displayLoading()

        logoutUseCase.getSingle().subscribeWith(object : DisposableSingleObserver<Unit>() {
            override fun onError(e: Throwable) {
                homeView.dismissLoading()
                homeView.navigateToLogin()
            }

            override fun onSuccess(t: Unit) {
                homeView.dismissLoading()
                homeView.navigateToLogin()
            }

        }).addTo(compositeDisposable)
    }

    fun getCategories(request: GetCategoriesRequest) {
        homeView.displayLoading()

        getCategoriesUseCase.getSingle(categoryViewModelMapper.map(request))
                .map { categoryViewModelMapper.map(it) }
                .subscribeWith(object : DisposableSingleObserver<List<CategoryViewModel>>() {
                    override fun onError(e: Throwable) {
                        homeView.displayError(errorViewModelMapper.map(e, ErrorAction.CUSTOM_ERROR_HANDLE))
                    }

                    override fun onSuccess(categoryViewModelList: List<CategoryViewModel>) {
                        homeView.dismissLoading()
                        homeView.displayCategories(categoryViewModelList)
                    }

                }).addTo(compositeDisposable)
    }

    fun setSelectedCategory(categoryViewModel: CategoryViewModel) {
        setSelectedCategoryUseCase.getSingle(categoryViewModelMapper.map(categoryViewModel)).subscribeWith(object : DisposableSingleObserver<Unit>() {
            override fun onError(e: Throwable) {
                homeView.displayError(errorViewModelMapper.map(e, ErrorAction.DISPLAY_TOAST))
            }

            override fun onSuccess(categoryViewModelList: Unit) {
                homeView.navigateToCategoryProtocol()
            }

        }).addTo(compositeDisposable)
    }

    fun shouldReloadCategories() {
        shouldReloadCategoriesUseCase.getSingle().subscribeWith(object : DisposableSingleObserver<Boolean>() {
            override fun onError(e: Throwable) {
                homeView.reloadCategories()
            }

            override fun onSuccess(shouldReloadCategories: Boolean) {
                if (shouldReloadCategories) {
                    homeView.reloadCategories()
                }
            }

        }).addTo(compositeDisposable)
    }
}