package com.privacy4all.presentation.scene.onboarding

import com.privacy4all.domain.use_case.GetUserInfoUseCase
import com.privacy4all.domain.use_case.SetOnBoardingAsDoneUseCase
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import com.privacy4all.presentation.common.mapper.ErrorViewModelMapper
import com.privacy4all.presentation.common.model.ErrorAction
import com.privacy4all.presentation.common.ui.BasePresenter
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@PerScene
class OnBoardingPresenter @Inject constructor(private val getUserInfoUseCase: GetUserInfoUseCase,
                                              private val setOnBoardingAsDoneUseCase: SetOnBoardingAsDoneUseCase,
                                              private val onBoardingViewModelMapper: OnBoardingViewModelMapper,
                                              private val errorViewModelMapper: ErrorViewModelMapper,
                                              private val onBoardingView: OnBoardingView) : BasePresenter() {

    fun getUserInfo() {
        onBoardingView.displayLoading()
        getUserInfoUseCase.getSingle()
                .map { onBoardingViewModelMapper.map(it) }
                .subscribeWith(object : DisposableSingleObserver<UserViewModel>() {
                    override fun onError(e: Throwable) {
                        onBoardingView.displayError(errorViewModelMapper.map(e, ErrorAction.DISPLAY_TOAST))
                    }

                    override fun onSuccess(userViewModel: UserViewModel) {
                        onBoardingView.dismissLoading()
                        onBoardingView.displayUser(userViewModel)
                    }

                }).addTo(compositeDisposable)
    }

    fun setOnBoardingAsDone() {
        setOnBoardingAsDoneUseCase.getSingle(true).subscribe()
    }
}