package com.privacy4all.presentation.scene.home

import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.ui.BaseView

interface HomeView : BaseView {
    fun displayUserPicture(userPictureUrl: String)
    fun navigateToLogin()
    fun displayCategories(categoryViewModelList: List<CategoryViewModel>)
    fun navigateToCategoryProtocol()
    fun reloadCategories()
}