package com.privacy4all.presentation.scene.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.privacy4all.R
import com.privacy4all.presentation.common.AndroidApplication
import com.privacy4all.presentation.common.Navigator
import com.privacy4all.presentation.common.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginView {

    @Inject
    lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerLoginComponent.builder()
                .applicationComponent(AndroidApplication.component)
                .loginModule(LoginModule(this, context!!))
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(loginWithFacebookButton).subscribe { loginPresenter.loginWithFacebook() }
        RxView.clicks(loginWithUuidButton).subscribe { loginPresenter.loginWithUuid() }
        loginPresenter.isUserLoggedIn()
    }

    override fun onDestroyView() {
        loginPresenter.destroy()
        super.onDestroyView()
    }

    override fun navigateToOnBoarding() {
        Navigator.openOnBoarding(context)
        finishWithoutAnimation()
    }

    override fun navigateToHome() {
        Navigator.openHome(context)
        finishWithoutAnimation()
    }

    override fun displayLoginButton() {
        loginWithFacebookButton.visibility = View.VISIBLE
        loginWithUuidButton.visibility = View.VISIBLE
    }

    private fun finishWithoutAnimation() {
        activity?.finish()
        activity?.overridePendingTransition(0,0)
    }

}