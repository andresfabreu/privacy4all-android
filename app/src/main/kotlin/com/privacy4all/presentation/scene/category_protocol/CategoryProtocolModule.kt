package com.privacy4all.presentation.scene.category_protocol

import android.content.Context
import com.privacy4all.data.network.data_source.AnswerApiDataSource
import com.privacy4all.data.network.data_source.AuthApiDataSource
import com.privacy4all.data.network.data_source.CategoryApiDataSource
import com.privacy4all.data.network.data_source.QuestionApiDataSource
import com.privacy4all.data.network.data_source_factory.AnswerApiDataSourceFactory
import com.privacy4all.data.network.data_source_factory.AuthApiDataSourceFactory
import com.privacy4all.data.network.data_source_factory.CategoryApiDataSourceFactory
import com.privacy4all.data.network.data_source_factory.QuestionApiDataSourceFactory
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import com.privacy4all.data.repository.AnswerRepository
import com.privacy4all.data.repository.AuthRepository
import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.data.repository.QuestionRepository
import com.privacy4all.domain.repository_data.AnswerDataRepository
import com.privacy4all.domain.repository_data.AuthDataRepository
import com.privacy4all.domain.repository_data.CategoryDataRepository
import com.privacy4all.domain.repository_data.QuestionDataRepository
import com.privacy4all.presentation.common.dependency_injection.qualifier.ActivityContext
import com.privacy4all.presentation.common.dependency_injection.scope.PerScene
import dagger.Module
import dagger.Provides

@Module
class CategoryProtocolModule(private val categoryProtocolView: CategoryProtocolView, private val context: Context) {

    @Provides
    @ActivityContext
    @PerScene
    fun provideActivityContext(): Context {
        return context
    }

    @Provides
    @PerScene
    internal fun provideCategoryProtocolView(): CategoryProtocolView {
        return categoryProtocolView
    }

    @Provides
    @PerScene
    fun provideAuthApiDataSource(sessionPreferencesDataSource: SessionPreferencesDataSource): AuthApiDataSource {
        return AuthApiDataSourceFactory(sessionPreferencesDataSource).authApiDataSource
    }

    @Provides
    @PerScene
    fun provideCategoryApiDataSource(sessionPreferencesDataSource: SessionPreferencesDataSource): CategoryApiDataSource {
        return CategoryApiDataSourceFactory(sessionPreferencesDataSource).categoryApiDataSource
    }

    @Provides
    @PerScene
    fun provideQuestionApiDataSource(sessionPreferencesDataSource: SessionPreferencesDataSource): QuestionApiDataSource {
        return QuestionApiDataSourceFactory(sessionPreferencesDataSource).questionApiDataSource
    }

    @Provides
    @PerScene
    fun provideAnswerApiDataSource(sessionPreferencesDataSource: SessionPreferencesDataSource): AnswerApiDataSource {
        return AnswerApiDataSourceFactory(sessionPreferencesDataSource).answerApiDataSource
    }

    @Provides
    @PerScene
    fun provideCategoryRepository(categoryRepository: CategoryRepository): CategoryDataRepository {
        return categoryRepository
    }

    @Provides
    @PerScene
    fun provideQuestionRepository(questionRepository: QuestionRepository): QuestionDataRepository {
        return questionRepository
    }

    @Provides
    @PerScene
    fun provideAnswerRepository(answerRepository: AnswerRepository): AnswerDataRepository {
        return answerRepository
    }

    @Provides
    @PerScene
    fun provideAuthRepository(authRepository: AuthRepository): AuthDataRepository {
        return authRepository
    }
}