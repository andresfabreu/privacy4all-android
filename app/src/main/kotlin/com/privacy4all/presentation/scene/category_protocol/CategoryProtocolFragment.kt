package com.privacy4all.presentation.scene.category_protocol

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.privacy4all.R
import com.privacy4all.presentation.common.AndroidApplication
import com.privacy4all.presentation.common.DEFAULT_PROTOCOL_ID
import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.model.ErrorViewModel
import com.privacy4all.presentation.common.ui.BaseFragment
import com.privacy4all.presentation.common.utils.loadUrlWithPlaceholder
import kotlinx.android.synthetic.main.custom_loading.*
import kotlinx.android.synthetic.main.fragment_category_protocol.*
import javax.inject.Inject

class CategoryProtocolFragment : BaseFragment(), CategoryProtocolView {

    @Inject
    lateinit var categoryProtocolPresenter: CategoryProtocolPresenter

    @ColorInt private var categoryColor: Int = 0
    private lateinit var questionAdapter: CategoryProtocolAdapter
    private var categoryId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerCategoryProtocolComponent.builder()
                .applicationComponent(AndroidApplication.component)
                .categoryProtocolModule(CategoryProtocolModule(this, context!!))
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_category_protocol, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryProtocolPresenter.getSelectedCategory()
        RxView.clicks(backButton).subscribe { activity?.onBackPressed() }
        RxView.clicks(retryButton).subscribe {
            errorLayout.visibility = View.GONE
            categoryProtocolPresenter.getQuestions(GetQuestionsRequest(DEFAULT_PROTOCOL_ID, categoryId))
        }
    }

    override fun onDestroyView() {
        categoryProtocolPresenter.destroy()
        super.onDestroyView()
    }

    override fun displayCustomError(errorViewModel: ErrorViewModel?) {
        errorLayout.visibility = View.VISIBLE
        val icon: Drawable? = ContextCompat.getDrawable(context!!, R.drawable.reload)
        icon?.mutate()?.setColorFilter(ContextCompat.getColor(context!!, R.color.white), PorterDuff.Mode.SRC_ATOP)
        retryButton.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
        errorMessage.text = errorViewModel?.message
    }

    override fun displayCategory(categoryViewModel: CategoryViewModel) {
        with(categoryViewModel) {
            categoryId = id
            categoryColor = Color.parseColor(colorHex)
            rootLayout.setBackgroundColor(categoryColor)
            categoryName.text = categoryViewModel.name
            categoryDescription.text = categoryViewModel.description
            categoryImage.loadUrlWithPlaceholder(categoryViewModel.imageUrl, R.drawable.ic_placeholder_categorias_detalhes)
            loadingProgress?.indeterminateDrawable?.mutate()?.setColorFilter(categoryColor, android.graphics.PorterDuff.Mode.SRC_ATOP)
            categoryProtocolPresenter.getQuestions(GetQuestionsRequest(DEFAULT_PROTOCOL_ID, categoryId))

            navigationLayout.visibility = View.VISIBLE
        }
    }

    override fun displayQuestion(questionViewModel: QuestionViewModel, currentQuestion: Int, totalQuestions: Int) {
        questionAdapter = CategoryProtocolAdapter(questionViewModel, categoryColor, currentQuestion, totalQuestions)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        questionRecycler.layoutManager = layoutManager
        questionRecycler.adapter = questionAdapter
        RxView.clicks(prevButton).subscribe { categoryProtocolPresenter.getPrevQuestion() }
        RxView.clicks(nextButton).subscribe { categoryProtocolPresenter.getNextQuestion() }
        RxView.clicks(doneButton).subscribe { activity?.onBackPressed() }
        questionAdapter.getOnAnswerClickObservable().subscribe {
            categoryProtocolPresenter.sendAnswer(SendAnswerRequest(DEFAULT_PROTOCOL_ID,
                    categoryId,
                    questionAdapter.getQuestionId(),
                    it.id))
        }
    }

    override fun updateQuestion(questionViewModel: QuestionViewModel, currentQuestion: Int) {
        questionAdapter.updateQuestionViewModel(questionViewModel, currentQuestion)
    }

    override fun hideNextButton() {
        nextButton.visibility = View.GONE
        doneButton.visibility = View.VISIBLE
    }

    override fun hidePrevButton() {
        prevButton.visibility = View.GONE
    }

    override fun showNextButton() {
        doneButton.visibility = View.GONE
        nextButton.visibility = View.VISIBLE
    }

    override fun showPrevButton() {
        prevButton.visibility = View.VISIBLE
    }
}