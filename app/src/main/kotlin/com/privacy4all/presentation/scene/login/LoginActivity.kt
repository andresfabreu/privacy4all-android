package com.privacy4all.presentation.scene.login

import android.os.Bundle
import com.privacy4all.R
import com.privacy4all.presentation.common.ui.BaseActivity

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        displayFragment(LoginFragment())
        setTransparentStatusBar()
    }

}