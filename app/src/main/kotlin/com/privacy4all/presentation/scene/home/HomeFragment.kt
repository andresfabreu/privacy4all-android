package com.privacy4all.presentation.scene.home

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.jakewharton.rxbinding2.view.RxView
import com.privacy4all.R
import com.privacy4all.presentation.common.AndroidApplication
import com.privacy4all.presentation.common.DEFAULT_PROTOCOL_ID
import com.privacy4all.presentation.common.Navigator
import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.model.ErrorViewModel
import com.privacy4all.presentation.common.model.GetCategoriesRequest
import com.privacy4all.presentation.common.ui.BaseActivity
import com.privacy4all.presentation.common.ui.BaseFragment
import com.privacy4all.presentation.common.utils.loadUrlAsCircle
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject


class HomeFragment : BaseFragment(), HomeView {

    @Inject
    lateinit var homePresenter: HomePresenter
    var categoryAdapter: HomeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerHomeComponent.builder()
                .applicationComponent(AndroidApplication.component)
                .homeModule(HomeModule(this, context!!))
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onResume() {
        super.onResume()
        homePresenter.shouldReloadCategories()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_home, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.logout -> {
                homePresenter.logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        homePresenter.destroy()
        super.onDestroyView()
    }

    private fun setupToolbar() {
        setHasOptionsMenu(true)

        with(activity as BaseActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            title = ""
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        homePresenter.getUserPicture()
        homePresenter.getCategories(GetCategoriesRequest(DEFAULT_PROTOCOL_ID))
        RxView.clicks(retryButton).subscribe {
            errorLayout.visibility = View.GONE
            homePresenter.getCategories(GetCategoriesRequest(DEFAULT_PROTOCOL_ID))
        }
    }

    override fun displayCustomError(errorViewModel: ErrorViewModel?) {
        errorLayout.visibility = View.VISIBLE
        errorMessage.text = errorViewModel?.message
    }

    override fun displayUserPicture(userPictureUrl: String) {
        toolbarImage.loadUrlAsCircle(userPictureUrl)
    }

    override fun navigateToLogin() {
        Navigator.openLogin(context)
    }

    override fun displayCategories(categoryViewModelList: List<CategoryViewModel>) {
        errorLayout.visibility = View.GONE
        if (categoryAdapter == null) {
            categoryAdapter = HomeAdapter(categoryViewModelList)
            categoryRecycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            categoryRecycler.adapter = categoryAdapter
            categoryAdapter?.getOnItemClickObservable()?.subscribe { homePresenter.setSelectedCategory(it) }
        } else {
            categoryAdapter?.updateViewModel(categoryViewModelList)
        }
    }

    override fun navigateToCategoryProtocol() {
        Navigator.openCategoryProtocol(context)
    }

    override fun reloadCategories() {
        homePresenter.getCategories(GetCategoriesRequest(DEFAULT_PROTOCOL_ID))
    }

}