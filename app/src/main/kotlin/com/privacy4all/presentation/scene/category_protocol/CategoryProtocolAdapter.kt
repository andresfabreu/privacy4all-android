package com.privacy4all.presentation.scene.category_protocol

import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.privacy4all.R
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_answer.*
import kotlinx.android.synthetic.main.item_question.*

class CategoryProtocolAdapter(private var questionViewModel: QuestionViewModel,
                              @ColorInt private val categoryColor: Int,
                              private var currentQuestion: Int = 0,
                              private val totalQuestions: Int = 0) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val QUESTION_TYPE: Int = 0
    private val ANSWER_TYPE: Int = 1

    private val onAnswerClickSubject: PublishSubject<AnswerViewModel> = PublishSubject.create()

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) QUESTION_TYPE else ANSWER_TYPE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (holder is QuestionViewHolder) {
            holder.bind(questionViewModel, currentQuestion, totalQuestions)
        } else if (holder is AnswerViewHolder) {
            holder.bind(questionViewModel, questionViewModel.answers[position - 1], itemCount - 1 == position, onAnswerClickSubject, categoryColor)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == QUESTION_TYPE) {
            return QuestionViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_question, parent, false))
        }

        return AnswerViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_answer, parent, false))
    }

    override fun getItemCount(): Int {
        return questionViewModel.answers.size + 1
    }

    fun getOnAnswerClickObservable(): Observable<AnswerViewModel> {
        return onAnswerClickSubject
    }

    fun updateQuestionViewModel(questionViewModel: QuestionViewModel, currentQuestion: Int) {
        this.currentQuestion = currentQuestion
        this.questionViewModel = questionViewModel
        notifyDataSetChanged()
    }

    fun getQuestionId(): Int {
        return questionViewModel.id
    }

    class QuestionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView = itemView

        fun bind(questionViewModel: QuestionViewModel, currentQuestion: Int, totalQuestions: Int) {
            with(questionViewModel) {
                currentPage.text = (currentQuestion + 1).toString()
                totalPage.text = String.format(" %s %s", containerView.context.getString(R.string.of), totalQuestions.toString())
                questionText.text = text
            }
        }
    }

    class AnswerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView = itemView

        fun bind(questionViewModel: QuestionViewModel, answerViewModel: AnswerViewModel, isLast: Boolean, onItemClickSubject: PublishSubject<AnswerViewModel>, @ColorInt categoryColor: Int) {
            with(answerViewModel) {
                answerText.text = text
                answerText.background = ContextCompat.getDrawable(containerView.context, if (isSelected) R.drawable.rounded_background_radius_6_white else R.drawable.rounded_background_radius_6_transparent)
                answerText.setTextColor(if (isSelected) categoryColor else ContextCompat.getColor(containerView.context, R.color.white))
                RxView.clicks(answerText).takeWhile { !isSelected || (questionViewModel.isMultipleChoice && questionViewModel.answers.count { it.isSelected } >= 2) }.map { this }.subscribe(onItemClickSubject)
                answerText.isEnabled = !isSelected || (questionViewModel.isMultipleChoice && questionViewModel.answers.count { it.isSelected } >= 2)
            }

            containerView.setPadding(0,
                    if (adapterPosition == 1) containerView.context.resources.getDimensionPixelOffset(R.dimen.category_padding) else 0,
                    0,
                    if (isLast) containerView.context.resources.getDimensionPixelOffset(R.dimen.answer_footer_padding) else containerView.context.resources.getDimensionPixelOffset(R.dimen.category_padding))
        }
    }

}