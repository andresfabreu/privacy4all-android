package com.privacy4all.presentation.common.mapper

abstract class ViewModelMapper<in Model, ViewModel> {

    abstract fun map(model: Model): ViewModel

    fun map(modelList: Collection<Model>) = modelList.map({ map(it) }).toMutableList()
}