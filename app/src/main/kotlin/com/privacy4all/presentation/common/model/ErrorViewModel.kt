package com.privacy4all.presentation.common.model


enum class ErrorAction {
    NONE,
    DISPLAY_TOAST,
    DISPLAY_DIALOG,
    DISPLAY_DIALOG_AND_NAVIGATE_BACK,
    CUSTOM_ERROR_HANDLE
}

data class ErrorViewModel(var message: String? = "", var action: ErrorAction? = ErrorAction.NONE)