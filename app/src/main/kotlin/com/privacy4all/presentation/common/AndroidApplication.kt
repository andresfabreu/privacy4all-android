package com.privacy4all.presentation.common

import android.app.Application
import com.facebook.FacebookSdk
import com.privacy4all.presentation.common.dependency_injection.component.ApplicationComponent
import com.privacy4all.presentation.common.dependency_injection.component.DaggerApplicationComponent
import com.privacy4all.presentation.common.dependency_injection.module.ApplicationModule

class AndroidApplication : Application() {

    companion object {
        lateinit var component: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()

        if (!FacebookSdk.isInitialized()) {
            FacebookSdk.sdkInitialize(applicationContext)
        }
    }
}