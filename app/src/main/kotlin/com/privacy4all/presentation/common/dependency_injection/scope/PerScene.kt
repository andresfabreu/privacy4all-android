package com.privacy4all.presentation.common.dependency_injection.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerScene