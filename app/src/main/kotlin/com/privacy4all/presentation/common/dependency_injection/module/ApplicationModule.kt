package com.privacy4all.presentation.common.dependency_injection.module

import android.content.Context
import android.content.SharedPreferences
import com.privacy4all.data.in_memory.data_source.InMemoryDataSource
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.presentation.common.AndroidApplication
import com.privacy4all.presentation.common.dependency_injection.qualifier.ApplicationContext
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @ApplicationContext
    fun provideApplicationContext(): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideSubscriberOnThreadExecutor(): ThreadExecutor {
        return ThreadExecutor(Schedulers.io())
    }

    @Provides
    @Singleton
    fun provideObserverOnExecutionThread(): PostThreadExecutor {
        return PostThreadExecutor(AndroidSchedulers.mainThread())
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences {
        return application.getSharedPreferences("app", Context.MODE_APPEND)
    }

    @Provides
    @Singleton
    fun provideInMemoryDataSource(): InMemoryDataSource {
        return InMemoryDataSource()
    }

}