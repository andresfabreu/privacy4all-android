package com.privacy4all.presentation.common.dependency_injection.component

import android.content.Context
import android.content.SharedPreferences
import com.privacy4all.data.in_memory.data_source.InMemoryDataSource
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.presentation.common.AndroidApplication
import com.privacy4all.presentation.common.dependency_injection.module.ApplicationModule
import com.privacy4all.presentation.common.dependency_injection.qualifier.ApplicationContext
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(androidApplication: AndroidApplication)

    @ApplicationContext
    fun context(): Context

    fun threadExecutor(): ThreadExecutor

    fun postThreadExecutor(): PostThreadExecutor

    fun sharedPreferences(): SharedPreferences

    fun inMemoryDataSource(): InMemoryDataSource
}