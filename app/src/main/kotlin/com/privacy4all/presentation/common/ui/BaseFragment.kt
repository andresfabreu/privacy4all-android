package com.privacy4all.presentation.common.ui

import android.content.DialogInterface
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import com.privacy4all.R
import com.privacy4all.presentation.common.model.ErrorAction
import com.privacy4all.presentation.common.model.ErrorViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.custom_loading.*


abstract class BaseFragment : Fragment(), BaseView {

    private val compositeDisposable = CompositeDisposable()
    private var errorDialog: AlertDialog? = null

    override fun onDestroyView() {
        compositeDisposable.dispose()
        errorDialog?.dismiss()
        super.onDestroyView()
    }

    override fun displayLoading() {
        loadingView?.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
        loadingView?.visibility = View.GONE
    }

    override fun displayError(errorViewModel: ErrorViewModel?) {
        dismissLoading()
        when (errorViewModel?.action) {
            ErrorAction.DISPLAY_DIALOG -> displayDialog(errorViewModel.message, null)
            ErrorAction.DISPLAY_TOAST -> displayToast(errorViewModel.message)
            ErrorAction.CUSTOM_ERROR_HANDLE -> displayCustomError(errorViewModel)
            else -> Unit
        }
    }

    override fun displayCustomError(errorViewModel: ErrorViewModel?) {

    }

    override fun displayToast(message: String?) {
        Toast.makeText(context, message ?: "", Toast.LENGTH_SHORT).show()
    }

    override fun displayDialog(message: String?, onPositiveClickListener: ((DialogInterface, Int) -> Unit)?) {
        if (errorDialog != null) {
            errorDialog?.dismiss()
            errorDialog = null
        }

        errorDialog = AlertDialog.Builder(this.context!!)
                .setMessage(message ?: "")
                .setCancelable(true)
                .setPositiveButton(resources.getString(R.string.ok), onPositiveClickListener ?: { dialog, _ -> dialog.dismiss() })
                .create()

        errorDialog?.show()
    }

    override fun displayDialogAndNavigateBack(message: String?) {
        displayDialog(message,
                { dialog, _ ->
                    dialog.dismiss()
                    activity?.onBackPressed()
                })
    }
}