package com.privacy4all.presentation.common.mapper

import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.GetCategoriesUseCase
import com.privacy4all.presentation.common.model.CategoryViewModel
import com.privacy4all.presentation.common.model.GetCategoriesRequest
import javax.inject.Inject

open class CategoryViewModelMapper @Inject constructor() : ViewModelMapper<Category, CategoryViewModel>() {
    override fun map(model: Category): CategoryViewModel {
        with(model) {
            return CategoryViewModel(id, name, description, iconUrl, imageUrl, colorHex, progress)
        }
    }

    fun map(categoryViewModel: CategoryViewModel): Category {
        with(categoryViewModel) {
            return Category(id, name, description, iconUrl, imageUrl, colorHex, progress)
        }
    }

    fun map(request: GetCategoriesRequest): GetCategoriesUseCase.Request {
        with(request) {
            return GetCategoriesUseCase.Request(protocolId)
        }
    }

}