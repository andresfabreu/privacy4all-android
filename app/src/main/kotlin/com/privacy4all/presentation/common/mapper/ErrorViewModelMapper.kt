package com.privacy4all.presentation.common.mapper

import android.content.Context
import com.privacy4all.R
import com.privacy4all.domain.exception.NoInternetException
import com.privacy4all.presentation.common.dependency_injection.qualifier.ActivityContext
import com.privacy4all.presentation.common.model.ErrorAction
import com.privacy4all.presentation.common.model.ErrorViewModel
import javax.inject.Inject

class ErrorViewModelMapper @Inject constructor(@ActivityContext private val context: Context) {
    fun map(throwable: Throwable, errorAction: ErrorAction?): ErrorViewModel {
        return when (throwable) {
            is NoInternetException -> ErrorViewModel(context.resources.getString(R.string.no_internet_error_message), errorAction)
            else -> ErrorViewModel(context.resources.getString(R.string.generic_error_message), errorAction)
        }
    }
}