package com.privacy4all.presentation.common.ui

import io.reactivex.disposables.CompositeDisposable

open class BasePresenter {
    protected val compositeDisposable = CompositeDisposable()
    open fun destroy() {
        compositeDisposable.dispose()
    }
}