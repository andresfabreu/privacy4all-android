package com.privacy4all.presentation.common.ui

import android.content.DialogInterface
import com.privacy4all.presentation.common.model.ErrorViewModel

interface BaseView {
    fun displayLoading()
    fun dismissLoading()
    fun displayError(errorViewModel: ErrorViewModel? = null)
    fun displayToast(message: String?)
    fun displayDialog(message: String?, onPositiveClickListener: ((DialogInterface, Int) -> Unit)?)
    fun displayDialogAndNavigateBack(message: String?)
    fun displayCustomError(errorViewModel: ErrorViewModel? = null)
}