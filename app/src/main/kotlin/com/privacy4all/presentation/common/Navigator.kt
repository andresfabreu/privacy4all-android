package com.privacy4all.presentation.common

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import com.privacy4all.presentation.scene.category_protocol.CategoryProtocolActivity
import com.privacy4all.presentation.scene.home.HomeActivity
import com.privacy4all.presentation.scene.login.LoginActivity
import com.privacy4all.presentation.scene.onboarding.OnBoardingActivity

object Navigator {
    fun openOnBoarding(context: Context?) {
        val intent = Intent(context, OnBoardingActivity::class.java)
        context?.startActivity(intent)
    }

    fun openHome(context: Context?) {
        val intent = Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    fun openLogin(context: Context?) {
        val intent = Intent(context, LoginActivity::class.java).setFlags(FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK)
        context?.startActivity(intent)
    }

    fun openCategoryProtocol(context: Context?) {
        val intent = Intent(context, CategoryProtocolActivity::class.java)
        context?.startActivity(intent)
    }
}