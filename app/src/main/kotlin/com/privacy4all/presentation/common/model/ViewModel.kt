package com.privacy4all.presentation.common.model

data class CategoryViewModel(var id: Int = 0,
                             var name: String = "",
                             var description: String = "",
                             var iconUrl: String = "",
                             var imageUrl: String = "",
                             var colorHex: String = "",
                             var progress: Int = 0)

data class GetCategoriesRequest(val protocolId: Int = 0)