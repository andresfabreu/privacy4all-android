package com.privacy4all.presentation.common.utils

import android.support.annotation.DrawableRes
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.widget.ImageView
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions

@GlideModule
class MyAppGlideModule : AppGlideModule()

fun asBold(string: String, start: Int, end: Int): SpannableStringBuilder {
    val spannableStringBuilder = SpannableStringBuilder(string)
    return if (start > string.length || end > string.length) {
        spannableStringBuilder
    } else {
        spannableStringBuilder.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableStringBuilder
    }
}

fun ImageView.loadUrlAsCircle(url: String) {
    GlideApp.with(context)
            .load(url)
            .apply(RequestOptions.bitmapTransform(CircleCrop()))
            .into(this)
}

fun ImageView.loadUrlWithPlaceholder(url: String, @DrawableRes drawableRes: Int) {
    GlideApp.with(context)
            .load(url)
            .placeholder(drawableRes)
            .into(this)
}