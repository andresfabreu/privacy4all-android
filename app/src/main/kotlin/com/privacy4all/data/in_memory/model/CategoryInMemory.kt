package com.privacy4all.data.in_memory.model

data class CategoryInMemory(var id: Int = 0,
                            var name: String = "",
                            var description: String = "",
                            var imageUrl: String = "",
                            var colorHex: String = "")