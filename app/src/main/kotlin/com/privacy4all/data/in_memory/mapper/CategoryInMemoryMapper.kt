package com.privacy4all.data.in_memory.mapper

import com.privacy4all.data.in_memory.model.CategoryInMemory
import com.privacy4all.domain.model.Category
import javax.inject.Inject

class CategoryInMemoryMapper @Inject constructor() {
    fun map(category: Category): CategoryInMemory {
        with(category) {
            return CategoryInMemory(id, name, description, imageUrl, colorHex)
        }
    }

    fun map(categoryInMemory: CategoryInMemory): Category {
        with(categoryInMemory) {
            return Category(id, name, description, "", imageUrl, colorHex)
        }
    }
}
