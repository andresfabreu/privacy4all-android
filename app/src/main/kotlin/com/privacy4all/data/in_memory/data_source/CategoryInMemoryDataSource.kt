package com.privacy4all.data.in_memory.data_source

import com.privacy4all.data.in_memory.model.CategoryInMemory
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class CategoryInMemoryDataSource @Inject constructor(private val inMemoryDataSource: InMemoryDataSource) {

    private val CATEGORY_KEY: String = "CATEGORY_KEY"
    private val SHOULD_RELOAD_CATEGORIES_KEY: String = "SHOULD_RELOAD_CATEGORIES_KEY"

    fun shouldReloadCategories(): Single<Boolean> {
        val obj = inMemoryDataSource.get(SHOULD_RELOAD_CATEGORIES_KEY)

        return if (obj is Boolean) {
            Single.just(obj as Boolean?)
        } else Single.just(false)

    }

    fun putShouldReloadCategories(shouldReloadCategories: Boolean): Single<Unit> {
        return Completable.fromAction { inMemoryDataSource.put(SHOULD_RELOAD_CATEGORIES_KEY, shouldReloadCategories) }.toSingle { Unit }
    }

    fun removeShouldReloadCategories(): Single<Unit> {
        return Completable.fromAction { inMemoryDataSource.remove(SHOULD_RELOAD_CATEGORIES_KEY) }.toSingle { Unit }
    }

    fun getCategory(): Single<CategoryInMemory> {
        val obj = inMemoryDataSource.get(CATEGORY_KEY)

        return if (obj is CategoryInMemory) {
            Single.just(obj as CategoryInMemory?)
        } else Single.just(CategoryInMemory())

    }

    fun putCategory(categoryInMemory: CategoryInMemory): Single<Unit> {
        return Completable.fromAction { inMemoryDataSource.put(CATEGORY_KEY, categoryInMemory) }.toSingle { Unit }
    }

    fun removeCategory(): Single<Unit> {
        return Completable.fromAction { inMemoryDataSource.remove(CATEGORY_KEY) }.toSingle { Unit }
    }

}
