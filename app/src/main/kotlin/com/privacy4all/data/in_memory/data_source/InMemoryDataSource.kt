package com.privacy4all.data.in_memory.data_source

import javax.inject.Inject

class InMemoryDataSource @Inject constructor() {

    private val storageMap: HashMap<String, Any> = HashMap()

    fun get(key: String): Any? {
        return storageMap[key]
    }

    fun put(key: String, model: Any) {
        storageMap.put(key, model)
    }

    fun remove(key: String) {
        storageMap.remove(key)
    }

    fun clear() {
        storageMap.clear()
    }
}