package com.privacy4all.data.network.mapper

import com.privacy4all.data.network.model.CategoryApi
import com.privacy4all.data.network.model.CategoryApiRequest
import com.privacy4all.data.network.model.QuestionApiRequest
import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.GetCategoriesUseCase
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import javax.inject.Inject

class CategoryApiMapper @Inject constructor() : ApiResponseMapper<Category, CategoryApi>() {
    override fun map(response: CategoryApi): Category {
        with(response) {
            return Category(id, name, description, iconUrl, imageUrl, colorHex, progress)
        }
    }

    fun map(useCaseRequest: GetCategoriesUseCase.Request): CategoryApiRequest {
        with(useCaseRequest) {
            return CategoryApiRequest(protocolId)
        }
    }
}
