package com.privacy4all.data.network.data_source

import com.privacy4all.data.network.model.QuestionApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface QuestionApiDataSource {
    @GET("question/list")
    fun getQuestions(@Query("protocol_id") protocolId: Int, @Query("category_id") categoryId: Int): Single<QuestionApiResponse>
}