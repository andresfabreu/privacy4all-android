package com.privacy4all.data.network.mapper

abstract class ApiResponseMapper<out Model, in Response> {

    abstract fun map(response: Response): Model

    fun map(modelList: Collection<Response>) = modelList.map({ map(it) })
}