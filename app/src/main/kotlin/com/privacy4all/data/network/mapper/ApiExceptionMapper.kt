package com.privacy4all.data.network.mapper

import com.privacy4all.domain.exception.NoInternetException
import com.privacy4all.domain.exception.UnauthorizedException
import com.privacy4all.domain.exception.UnexpectedException
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

object ApiExceptionMapper {

    fun map(throwable: Throwable): Throwable {
        if (throwable is HttpException) {
            val response = throwable.response()
            return when (response.code()) {
                HttpURLConnection.HTTP_UNAUTHORIZED -> UnauthorizedException()
                else -> UnexpectedException()
            }
        }

        // A timeout happened
        if (throwable is SocketTimeoutException) return TimeoutException()

        // A network error happened
        return if (throwable is IOException) NoInternetException() else throwable
    }
}