package com.privacy4all.data.network.data_source

import com.privacy4all.data.network.model.LoginApiRequest
import com.privacy4all.data.network.model.LoginApiResponse
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthApiDataSource {
    @POST("auth/facebook")
    fun loginWithFacebook(@Body loginRequest: LoginApiRequest): Single<LoginApiResponse>

    @POST("auth/uuid")
    fun loginWithUuid(@Body loginRequest: LoginApiRequest): Single<LoginApiResponse>

    @GET("auth/logout")
    fun logout(): Single<Result<Unit>>
}