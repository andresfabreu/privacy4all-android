package com.privacy4all.data.network.mapper

import com.privacy4all.data.network.model.AnswerApi
import com.privacy4all.data.network.model.AnswerApiRequest
import com.privacy4all.domain.model.Answer
import com.privacy4all.domain.use_case.SendAnswerUseCase
import javax.inject.Inject

class AnswerApiMapper @Inject constructor() : ApiResponseMapper<Answer, AnswerApi>() {
    override fun map(response: AnswerApi): Answer {
        with(response) {
            return Answer(id, text, isSelected)
        }
    }

    fun map(useCaseRequest: SendAnswerUseCase.Request): AnswerApiRequest {
        with(useCaseRequest) {
            return AnswerApiRequest(protocolId, categoryId, questionId, id)
        }
    }
}