package com.privacy4all.data.network.model

data class QuestionApiRequest(val protocolId: Int = 1,
                              val categoryId: Int = 0)

data class QuestionApiResponse(var questions: List<QuestionApi> = ArrayList())

data class QuestionApi(var id: Int = 0,
                       var text: String = "",
                       var isMultipleChoice: Boolean = false,
                       var answers: List<AnswerApi> = ArrayList())