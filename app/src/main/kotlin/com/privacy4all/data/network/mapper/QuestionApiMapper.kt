package com.privacy4all.data.network.mapper

import com.privacy4all.data.network.model.QuestionApi
import com.privacy4all.data.network.model.QuestionApiRequest
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import javax.inject.Inject

class QuestionApiMapper @Inject constructor(private val answerApiMapper: AnswerApiMapper) : ApiResponseMapper<Question, QuestionApi>() {
    override fun map(response: QuestionApi): Question {
        with(response) {
            return Question(id, text, isMultipleChoice, answerApiMapper.map(answers))
        }
    }

    fun map(useCaseRequest: GetQuestionsUseCase.Request): QuestionApiRequest {
        with(useCaseRequest) {
            return QuestionApiRequest(protocolId, categoryId)
        }
    }
}