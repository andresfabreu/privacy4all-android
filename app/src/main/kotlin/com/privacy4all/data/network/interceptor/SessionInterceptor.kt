package com.privacy4all.data.network.interceptor

import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class SessionInterceptor(private val sessionPreferencesDataSource: SessionPreferencesDataSource) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val request = createRequest(original)
        return chain.proceed(request)
    }

    private fun createRequest(original: Request): Request {
        val builder = original.newBuilder()
        addHeader(builder, "Authorization", sessionPreferencesDataSource.getSessionToken().blockingGet())
        return builder.build()
    }

    private fun addHeader(builder: Request.Builder?, key: String, value: String?) {
        if (value != null) {
            builder?.addHeader(key, value)
        }
    }
}