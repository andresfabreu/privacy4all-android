package com.privacy4all.data.network.model

data class CategoryApiRequest(val protocolId: Int = 1)

data class CategoryApiResponse(var categories: List<CategoryApi> = ArrayList())

data class CategoryApi(var id: Int = 0,
                       var name: String = "",
                       var description: String = "",
                       var iconUrl: String = "",
                       var imageUrl: String = "",
                       var colorHex: String = "",
                       var progress: Int = 0)