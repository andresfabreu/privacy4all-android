package com.privacy4all.data.network.model

data class AnswerApiRequest(val protocolId: Int = 1,
                            val categoryId: Int = 0,
                            val questionId: Int = 0,
                            val id: Int = 0)

data class AnswerApi(var id: Int = 0,
                     var text: String = "",
                     var isSelected: Boolean = false)