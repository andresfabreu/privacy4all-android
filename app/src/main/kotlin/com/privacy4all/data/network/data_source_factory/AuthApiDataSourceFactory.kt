package com.privacy4all.data.network.data_source_factory

import com.privacy4all.data.network.data_source.AuthApiDataSource
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import javax.inject.Inject

class AuthApiDataSourceFactory @Inject constructor(sessionPersistentDataSource: SessionPreferencesDataSource) : ApiDataSourceFactory() {
    val authApiDataSource: AuthApiDataSource = build(sessionPersistentDataSource).create(AuthApiDataSource::class.java)
}