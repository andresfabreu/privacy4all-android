package com.privacy4all.data.network.model

data class LoginApiRequest(val token: String = "")

data class LoginApiResponse(var id: Long = 0,
                            var name: String? = "",
                            var username: String = "",
                            var pictureUrl: String? = "",
                            var accessToken: String = "")