package com.privacy4all.data.network.data_source

import com.privacy4all.data.network.model.CategoryApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CategoryApiDataSource {
    @GET("category/list")
    fun getCategories(@Query("protocol_id") protocolId : Int): Single<CategoryApiResponse>
}