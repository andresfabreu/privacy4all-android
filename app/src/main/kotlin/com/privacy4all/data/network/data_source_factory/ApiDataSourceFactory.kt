package com.privacy4all.data.network.data_source_factory

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.privacy4all.BuildConfig
import com.privacy4all.data.network.call_adapter_factory.ApiErrorHandlingCallAdapterFactory
import com.privacy4all.data.network.interceptor.SessionInterceptor
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class ApiDataSourceFactory {
    fun build(sessionPersistentDataSource: SessionPreferencesDataSource): Retrofit {
        val builder = OkHttpClient.Builder().addInterceptor(SessionInterceptor(sessionPersistentDataSource))

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }

        val client = builder.build()

        val retrofitBuilder = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .addCallAdapterFactory(ApiErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(defaultConverterFactory())

        return retrofitBuilder.build()
    }

    private fun defaultConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create())
    }
}