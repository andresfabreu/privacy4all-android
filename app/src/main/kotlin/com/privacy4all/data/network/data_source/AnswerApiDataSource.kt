package com.privacy4all.data.network.data_source

import com.privacy4all.data.network.model.AnswerApiRequest
import com.privacy4all.data.network.model.QuestionApiResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.PUT

interface AnswerApiDataSource {
    @PUT("answer")
    fun senAnswer(@Body answerApiRequest: AnswerApiRequest): Single<QuestionApiResponse>
}