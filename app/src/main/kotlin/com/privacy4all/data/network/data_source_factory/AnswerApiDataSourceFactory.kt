package com.privacy4all.data.network.data_source_factory

import com.privacy4all.data.network.data_source.AnswerApiDataSource
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import javax.inject.Inject

class AnswerApiDataSourceFactory @Inject constructor(sessionPersistentDataSource: SessionPreferencesDataSource) : ApiDataSourceFactory() {
    val answerApiDataSource: AnswerApiDataSource = build(sessionPersistentDataSource).create(AnswerApiDataSource::class.java)
}