package com.privacy4all.data.network.data_source_factory

import com.privacy4all.data.network.data_source.CategoryApiDataSource
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import javax.inject.Inject

class CategoryApiDataSourceFactory @Inject constructor(sessionPersistentDataSource: SessionPreferencesDataSource) : ApiDataSourceFactory() {
    val categoryApiDataSource: CategoryApiDataSource = build(sessionPersistentDataSource).create(CategoryApiDataSource::class.java)
}