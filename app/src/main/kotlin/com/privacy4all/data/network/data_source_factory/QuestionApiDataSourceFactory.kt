package com.privacy4all.data.network.data_source_factory

import com.privacy4all.data.network.data_source.QuestionApiDataSource
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import javax.inject.Inject

class QuestionApiDataSourceFactory @Inject constructor(sessionPersistentDataSource: SessionPreferencesDataSource) : ApiDataSourceFactory() {
    val questionApiDataSource: QuestionApiDataSource = build(sessionPersistentDataSource).create(QuestionApiDataSource::class.java)
}