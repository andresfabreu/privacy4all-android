package com.privacy4all.data.repository

import com.privacy4all.data.in_memory.data_source.CategoryInMemoryDataSource
import com.privacy4all.data.in_memory.mapper.CategoryInMemoryMapper
import com.privacy4all.data.network.data_source.CategoryApiDataSource
import com.privacy4all.data.network.mapper.CategoryApiMapper
import com.privacy4all.data.network.model.CategoryApiRequest
import com.privacy4all.domain.model.Category
import com.privacy4all.domain.repository_data.CategoryDataRepository
import com.privacy4all.domain.use_case.GetCategoriesUseCase
import io.reactivex.Single
import javax.inject.Inject

class CategoryRepository @Inject constructor(private val categoryApiDataSource: CategoryApiDataSource,
                                             private val categoryInMemoryDataSource: CategoryInMemoryDataSource,
                                             private val categoryApiMapper: CategoryApiMapper,
                                             private val categoryInMemoryMapper: CategoryInMemoryMapper) : CategoryDataRepository {

    override fun getCategories(request: GetCategoriesUseCase.Request): Single<List<Category>> {
        return categoryApiDataSource.getCategories(CategoryApiRequest().protocolId)
                .map { categoryApiMapper.map(it.categories) }
                .flatMap { categories -> categoryInMemoryDataSource.removeShouldReloadCategories().map { categories } }
    }

    override fun setCategoryAsSelected(category: Category): Single<Unit> {
        return categoryInMemoryDataSource.putCategory(categoryInMemoryMapper.map(category))
    }

    override fun getSelectedCategory(): Single<Category> {
        return categoryInMemoryDataSource.getCategory()
                .map { categoryInMemoryMapper.map(it) }
                .flatMap { category -> categoryInMemoryDataSource.removeCategory().map { category } }
    }

    override fun shouldReloadCategories(): Single<Boolean> {
        return categoryInMemoryDataSource.shouldReloadCategories()
                .flatMap { shouldReloadCategories -> categoryInMemoryDataSource.removeShouldReloadCategories().map { shouldReloadCategories } }
    }
}