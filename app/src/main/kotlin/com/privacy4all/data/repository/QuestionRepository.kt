package com.privacy4all.data.repository

import com.privacy4all.data.network.data_source.QuestionApiDataSource
import com.privacy4all.data.network.mapper.QuestionApiMapper
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.repository_data.QuestionDataRepository
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import io.reactivex.Single
import javax.inject.Inject

class QuestionRepository @Inject constructor(private val questionApiDataSource: QuestionApiDataSource,
                                             private val questionApiMapper: QuestionApiMapper) : QuestionDataRepository {

    override fun getQuestions(request: GetQuestionsUseCase.Request): Single<List<Question>> {
        val apiRequest = questionApiMapper.map(request)
        return questionApiDataSource.getQuestions(apiRequest.protocolId, apiRequest.categoryId).map { questionApiMapper.map(it.questions) }
    }
}