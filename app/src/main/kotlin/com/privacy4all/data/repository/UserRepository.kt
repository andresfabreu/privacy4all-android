package com.privacy4all.data.repository

import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import com.privacy4all.data.preferences.data_source.UserPreferencesDataSource
import com.privacy4all.data.preferences.mapper.UserPrefMapper
import com.privacy4all.domain.model.User
import com.privacy4all.domain.repository_data.UserDataRepository
import io.reactivex.Single
import javax.inject.Inject

class UserRepository @Inject constructor(private val userPreferencesDataSource: UserPreferencesDataSource,
                                         private val sessionPreferencesDataSource: SessionPreferencesDataSource,
                                         private val userPrefMapper: UserPrefMapper) : UserDataRepository {
    override fun setOnBoardingAsDone(params: Boolean?): Single<Unit> {
        return userPreferencesDataSource.putIsOnBoardingDone(params ?: false)
    }

    override fun isUserLoggedIn(): Single<Boolean> {
        return sessionPreferencesDataSource.getSessionToken().map { it.isNotEmpty() }
    }

    override fun isOnBoardingDone(): Single<Boolean> {
        return userPreferencesDataSource.isOnBoardingDone()
    }

    override fun getUserInfo(): Single<User> {
        return userPreferencesDataSource.getUserPref().map { userPrefMapper.map(it) }
    }

}