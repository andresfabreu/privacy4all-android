package com.privacy4all.data.repository

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.provider.Settings
import com.privacy4all.data.common.HashUtils
import com.privacy4all.data.network.data_source.AuthApiDataSource
import com.privacy4all.data.network.model.LoginApiRequest
import com.privacy4all.data.preferences.data_source.DevicePreferencesDataSource
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import com.privacy4all.data.preferences.data_source.UserPreferencesDataSource
import com.privacy4all.data.preferences.mapper.UserPrefMapper
import com.privacy4all.domain.repository_data.AuthDataRepository
import com.privacy4all.presentation.common.dependency_injection.qualifier.ActivityContext
import com.u.rxfacebook.RxFacebook
import io.reactivex.Single
import javax.inject.Inject


class AuthRepository @Inject constructor(private val authApiDataSource: AuthApiDataSource,
                                         private val sessionPreferencesDataSource: SessionPreferencesDataSource,
                                         private val userPreferencesDataSource: UserPreferencesDataSource,
                                         private val devicePreferencesDataSource: DevicePreferencesDataSource,
                                         private val userPrefMapper: UserPrefMapper,
                                         @ActivityContext private val context: Context) : AuthDataRepository {

    override fun logout(): Single<Unit> {
        return authApiDataSource.logout()
                .flatMap { sessionPreferencesDataSource.clearSessionPreferences() }
                .flatMap { userPreferencesDataSource.clearUserPreferences() }
                .flatMap { devicePreferencesDataSource.clearDevicePreferences() }
    }

    override fun loginWithFacebook(): Single<Unit> {
        return Single.fromCallable { RxFacebook.create().logout().blockingSingle() }
                .flatMap {
                    authApiDataSource.loginWithFacebook(LoginApiRequest(RxFacebook.create().loginWithPublishPermissions(context as Activity, ArrayList()).blockingSingle().accessToken.token))
                            .flatMap { resp -> userPreferencesDataSource.putUserPref(userPrefMapper.map(resp)).map { resp } }
                            .flatMap { resp -> sessionPreferencesDataSource.putSessionToken(resp.accessToken) }
                            .map { Unit }
                }
    }

    @SuppressLint("HardwareIds")
    override fun loginWithUuid(uuid: String?): Single<Unit> {
        return devicePreferencesDataSource.putDeviceId(uuid
                ?: HashUtils.md5(Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)))
                .flatMap { deviceId -> authApiDataSource.loginWithUuid(LoginApiRequest(deviceId)) }
                .flatMap { resp -> userPreferencesDataSource.putUserPref(userPrefMapper.map(resp)).map { resp } }
                .flatMap { resp -> sessionPreferencesDataSource.putSessionToken(resp.accessToken) }
                .map { Unit }
    }

    override fun refreshSession(uuid: String?): Single<Unit> {
        return if (devicePreferencesDataSource.getDeviceId().blockingGet().isNullOrEmpty()) loginWithFacebook() else loginWithUuid(uuid)
    }

}