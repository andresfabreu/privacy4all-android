package com.privacy4all.data.repository

import com.privacy4all.data.in_memory.data_source.CategoryInMemoryDataSource
import com.privacy4all.data.network.data_source.AnswerApiDataSource
import com.privacy4all.data.network.mapper.AnswerApiMapper
import com.privacy4all.data.network.mapper.QuestionApiMapper
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.repository_data.AnswerDataRepository
import com.privacy4all.domain.use_case.SendAnswerUseCase
import io.reactivex.Single
import javax.inject.Inject

class AnswerRepository @Inject constructor(private val answerApiDataSource: AnswerApiDataSource,
                                           private val categoryInMemoryDataSource: CategoryInMemoryDataSource,
                                           private val questionApiMapper: QuestionApiMapper,
                                           private val answerApiMapper: AnswerApiMapper) : AnswerDataRepository {

    override fun sendAnswer(request: SendAnswerUseCase.Request): Single<List<Question>> {
        return answerApiDataSource.senAnswer(answerApiMapper.map(request))
                .map { questionApiMapper.map(it.questions) }
                .flatMap { questions -> categoryInMemoryDataSource.putShouldReloadCategories(true).map { questions } }
    }
}