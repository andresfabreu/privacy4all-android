package com.privacy4all.data.preferences.data_source

import android.content.SharedPreferences
import io.reactivex.Single
import javax.inject.Inject

class DevicePreferencesDataSource @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private val DEVICE_ID_KEY: String = "DEVICE_ID_KEY"

    fun getDeviceId(): Single<String> {
        return Single.fromCallable { sharedPreferences.getString(DEVICE_ID_KEY, "") }
    }

    fun putDeviceId(deviceId: String): Single<String> {
        return Single.fromCallable { sharedPreferences.edit().putString(DEVICE_ID_KEY, deviceId).apply() }.map { deviceId }
    }

    fun clearDevicePreferences(): Single<Unit> {
        return Single.fromCallable { sharedPreferences.edit().remove(DEVICE_ID_KEY).apply() }
    }
}