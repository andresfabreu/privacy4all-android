package com.privacy4all.data.preferences.model

data class UserPref(val name: String = "",
                    val pictureUrl: String = "")