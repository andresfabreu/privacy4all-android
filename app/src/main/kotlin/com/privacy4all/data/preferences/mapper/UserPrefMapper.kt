package com.privacy4all.data.preferences.mapper

import com.privacy4all.data.network.model.LoginApiResponse
import com.privacy4all.data.preferences.model.UserPref
import com.privacy4all.domain.model.User
import javax.inject.Inject

class UserPrefMapper @Inject constructor() {
    fun map(response: LoginApiResponse): UserPref {
        with(response) {
            return UserPref(name?: "", pictureUrl?: "")
        }
    }

    fun map(userPref: UserPref): User {
        with(userPref) {
            return User(name, pictureUrl)
        }
    }
}