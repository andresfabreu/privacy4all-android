package com.privacy4all.data.preferences.data_source

import android.content.SharedPreferences
import com.google.gson.Gson
import com.privacy4all.data.preferences.model.UserPref
import io.reactivex.Single
import javax.inject.Inject

class UserPreferencesDataSource @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private val USER_PREF_KEY: String = "USER_PREF_KEY"
    private val IS_ONBOARDIN_DONE_KEY: String = "IS_ONBOARDIN_DONE_KEY"
    private val gson: Gson = Gson()

    fun getUserPref(): Single<UserPref> {
        return Single.fromCallable { gson.fromJson(sharedPreferences.getString(USER_PREF_KEY, null), UserPref::class.java) ?: UserPref() }
    }

    fun putUserPref(userPref: UserPref?): Single<Unit> {
        return Single.fromCallable { sharedPreferences.edit().putString(USER_PREF_KEY, gson.toJson(userPref ?: UserPref())).apply() }
    }

    fun isOnBoardingDone(): Single<Boolean> {
        return Single.fromCallable { sharedPreferences.getBoolean(IS_ONBOARDIN_DONE_KEY, false) }
    }

    fun putIsOnBoardingDone(isOnBoardingDone: Boolean): Single<Unit> {
        return Single.fromCallable { sharedPreferences.edit().putBoolean(IS_ONBOARDIN_DONE_KEY, isOnBoardingDone).apply() }
    }

    fun clearUserPreferences(): Single<Unit> {
        return Single.fromCallable { sharedPreferences.edit().remove(USER_PREF_KEY).apply() }
    }
}