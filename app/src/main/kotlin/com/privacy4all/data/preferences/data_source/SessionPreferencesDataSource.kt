package com.privacy4all.data.preferences.data_source

import android.content.SharedPreferences
import io.reactivex.Single
import javax.inject.Inject

class SessionPreferencesDataSource @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private val SESSION_TOKEN_KEY: String = "SESSION_TOKEN_KEY"

    fun getSessionToken(): Single<String> {
        return Single.fromCallable { sharedPreferences.getString(SESSION_TOKEN_KEY, "") ?: "" }
    }

    fun putSessionToken(sessionToken: String?): Single<Unit> {
        return Single.fromCallable { sharedPreferences.edit().putString(SESSION_TOKEN_KEY, sessionToken ?: "").apply() }
    }

    fun clearSessionPreferences(): Single<Unit> {
        return Single.fromCallable { sharedPreferences.edit().remove(SESSION_TOKEN_KEY).apply() }
    }
}