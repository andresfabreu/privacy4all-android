package com.privacy4all.domain.model

data class Category(var id: Int = 0,
                    var name: String = "",
                    var description: String = "",
                    var iconUrl: String = "",
                    var imageUrl: String = "",
                    var colorHex: String = "",
                    var progress: Int = 0)