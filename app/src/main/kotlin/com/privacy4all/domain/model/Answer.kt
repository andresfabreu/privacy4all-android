package com.privacy4all.domain.model

data class Answer(var id: Int = 0,
                  var text: String = "",
                  var isSelected: Boolean = false)