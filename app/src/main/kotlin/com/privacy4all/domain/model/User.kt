package com.privacy4all.domain.model

data class User(var name: String = "",
                var pictureUrl: String = "")