package com.privacy4all.domain.model

data class Question(var id: Int = 0,
                    var text: String = "",
                    var isMultipleChoice: Boolean = false,
                    var answers: List<Answer> = ArrayList())

