package com.privacy4all.domain.exception

class NoInternetException : RuntimeException()