package com.privacy4all.domain.exception

class UnauthorizedException : RuntimeException()