package com.privacy4all.domain.repository_data

import io.reactivex.Single

interface AuthDataRepository {
    fun loginWithFacebook(): Single<Unit>
    fun logout(): Single<Unit>
    fun loginWithUuid(uuid: String?): Single<Unit>
    fun refreshSession(uuid: String?): Single<Unit>
}