package com.privacy4all.domain.repository_data

import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import io.reactivex.Single

interface QuestionDataRepository {
    fun getQuestions(request: GetQuestionsUseCase.Request): Single<List<Question>>
}