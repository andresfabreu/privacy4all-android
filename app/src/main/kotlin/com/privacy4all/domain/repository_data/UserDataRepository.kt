package com.privacy4all.domain.repository_data

import com.privacy4all.domain.model.User
import io.reactivex.Single

interface UserDataRepository {
    fun getUserInfo(): Single<User>
    fun isOnBoardingDone(): Single<Boolean>
    fun isUserLoggedIn(): Single<Boolean>
    fun setOnBoardingAsDone(params: Boolean?): Single<Unit>
}