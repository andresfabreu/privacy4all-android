package com.privacy4all.domain.repository_data

import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.GetCategoriesUseCase
import io.reactivex.Single

interface CategoryDataRepository {
    fun getCategories(request: GetCategoriesUseCase.Request): Single<List<Category>>
    fun setCategoryAsSelected(category: Category): Single<Unit>
    fun getSelectedCategory(): Single<Category>
    fun shouldReloadCategories() : Single<Boolean>
}