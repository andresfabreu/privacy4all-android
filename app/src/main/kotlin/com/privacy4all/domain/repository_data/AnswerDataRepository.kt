package com.privacy4all.domain.repository_data

import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.SendAnswerUseCase
import io.reactivex.Single

interface AnswerDataRepository {
    fun sendAnswer(request: SendAnswerUseCase.Request): Single<List<Question>>
}