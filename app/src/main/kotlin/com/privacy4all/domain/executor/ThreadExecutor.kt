package com.privacy4all.domain.executor

import io.reactivex.Scheduler

class ThreadExecutor(val scheduler: Scheduler)