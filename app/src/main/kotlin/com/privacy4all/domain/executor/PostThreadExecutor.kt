package com.privacy4all.domain.executor

import io.reactivex.Scheduler

class PostThreadExecutor(val scheduler: Scheduler)