package com.privacy4all.domain.use_case

import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.repository_data.UserDataRepository
import io.reactivex.Single
import javax.inject.Inject

class IsUserLoggedInUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                                observerOn: PostThreadExecutor,
                                                private val userDataRepository: UserDataRepository) : UseCase<Boolean, Unit>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Unit?): Single<Boolean> {
        return userDataRepository.isUserLoggedIn()
    }
}
