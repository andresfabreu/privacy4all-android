package com.privacy4all.domain.use_case

import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.repository_data.UserDataRepository
import io.reactivex.Single
import javax.inject.Inject

class SetOnBoardingAsDoneUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                                     observerOn: PostThreadExecutor,
                                                     private val userDataRepository: UserDataRepository) : UseCase<Unit, Boolean>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Boolean?): Single<Unit> {
        return userDataRepository.setOnBoardingAsDone(params)
    }
}