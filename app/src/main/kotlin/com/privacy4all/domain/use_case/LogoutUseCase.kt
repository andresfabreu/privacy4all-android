package com.privacy4all.domain.use_case

import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.repository_data.AuthDataRepository
import io.reactivex.Single
import javax.inject.Inject

class LogoutUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                        observerOn: PostThreadExecutor,
                                        private val authDataRepository: AuthDataRepository) : UseCase<Unit, Unit>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Unit?): Single<Unit> {
        return authDataRepository.logout()
    }
}