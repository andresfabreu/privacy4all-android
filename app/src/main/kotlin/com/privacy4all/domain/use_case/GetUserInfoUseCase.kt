package com.privacy4all.domain.use_case

import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.model.User
import com.privacy4all.domain.repository_data.UserDataRepository
import io.reactivex.Single
import javax.inject.Inject

class GetUserInfoUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                             observerOn: PostThreadExecutor,
                                             private val userDataRepository: UserDataRepository) : UseCase<User, Unit>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Unit?): Single<User> {
        return userDataRepository.getUserInfo()
    }
}