package com.privacy4all.domain.use_case

import com.privacy4all.data.repository.QuestionRepository
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.model.Question
import io.reactivex.Single
import javax.inject.Inject

class GetQuestionsUseCase @Inject constructor(refreshSessionUseCase: RefreshSessionUseCase,
                                              subscriberOn: ThreadExecutor,
                                              observerOn: PostThreadExecutor,
                                              private val questionRepository: QuestionRepository) : UseCase<List<Question>, GetQuestionsUseCase.Request>(refreshSessionUseCase, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: GetQuestionsUseCase.Request?): Single<List<Question>> {
        return questionRepository.getQuestions(params ?: Request())
    }

    class Request(val protocolId: Int = 0,
                  val categoryId: Int = 0)
}