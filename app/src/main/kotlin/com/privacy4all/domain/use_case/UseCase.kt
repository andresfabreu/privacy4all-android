package com.privacy4all.domain.use_case

import com.privacy4all.domain.exception.UnauthorizedException
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver


abstract class UseCase<Model, in Params> internal constructor(private val refreshSessionUseCase: RefreshSessionUseCase?,
                                                              private val subscriberOn: ThreadExecutor,
                                                              private val observerOn: PostThreadExecutor) {

    protected abstract fun buildUseCaseSingle(params: Params? = null): Single<Model>

    fun getSingle(params: Params? = null): Single<Model> {
        return this.buildUseCaseSingle(params)
                .subscribeOn(subscriberOn.scheduler)
                .observeOn(observerOn.scheduler)
                .retryWhen { ex ->
                    ex.flatMap { e ->
                        if (e is UnauthorizedException && refreshSessionUseCase != null) {
                            return@flatMap Observable.create<Unit> { subscriber ->
                                refreshSessionUseCase.getSingle().subscribeWith(object : DisposableSingleObserver<Unit>() {
                                    override fun onError(e: Throwable) {
                                        subscriber.onError(e)
                                    }

                                    override fun onSuccess(t: Unit) {
                                        subscriber.onNext(Unit)
                                    }

                                })
                            }.toFlowable(BackpressureStrategy.LATEST)
                        } else {
                            return@flatMap Flowable.error<Model>(e)
                        }
                    }
                }
    }
}