package com.privacy4all.domain.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import io.reactivex.Single
import javax.inject.Inject

class ShouldReloadCategoriesUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                                        observerOn: PostThreadExecutor,
                                                        private val categoryRepository: CategoryRepository) : UseCase<Boolean, Unit>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Unit?): Single<Boolean> {
        return categoryRepository.shouldReloadCategories()
    }
}