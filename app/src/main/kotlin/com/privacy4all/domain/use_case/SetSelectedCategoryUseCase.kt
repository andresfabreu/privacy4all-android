package com.privacy4all.domain.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.model.Category
import io.reactivex.Single
import javax.inject.Inject

class SetSelectedCategoryUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                                     observerOn: PostThreadExecutor,
                                                     private val categoryRepository: CategoryRepository) : UseCase<Unit, Category>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Category?): Single<Unit> {
        return categoryRepository.setCategoryAsSelected(params ?: Category())
    }
}