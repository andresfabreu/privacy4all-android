package com.privacy4all.domain.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.model.Category
import io.reactivex.Single
import javax.inject.Inject

class GetSelectedCategoryUseCase @Inject constructor(subscriberOn: ThreadExecutor,
                                                     observerOn: PostThreadExecutor,
                                                     private val categoryRepository: CategoryRepository) : UseCase<Category, Unit>(null, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: Unit?): Single<Category> {
        return categoryRepository.getSelectedCategory()
    }
}