package com.privacy4all.domain.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.model.Category
import io.reactivex.Single
import javax.inject.Inject

class GetCategoriesUseCase @Inject constructor(refreshSessionUseCase: RefreshSessionUseCase,
                                               subscriberOn: ThreadExecutor,
                                               observerOn: PostThreadExecutor,
                                               private val categoryRepository: CategoryRepository) : UseCase<List<Category>, GetCategoriesUseCase.Request>(refreshSessionUseCase, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: GetCategoriesUseCase.Request?): Single<List<Category>> {
        return categoryRepository.getCategories(params?: Request())
    }

    class Request(val protocolId: Int = 0)
}