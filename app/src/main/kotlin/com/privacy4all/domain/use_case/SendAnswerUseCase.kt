package com.privacy4all.domain.use_case

import com.privacy4all.data.repository.AnswerRepository
import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.model.Question
import io.reactivex.Single
import javax.inject.Inject

class SendAnswerUseCase @Inject constructor(refreshSessionUseCase: RefreshSessionUseCase,
                                            subscriberOn: ThreadExecutor,
                                            observerOn: PostThreadExecutor,
                                            private val answerRepository: AnswerRepository) : UseCase<List<Question>, SendAnswerUseCase.Request>(refreshSessionUseCase, subscriberOn, observerOn) {
    override fun buildUseCaseSingle(params: SendAnswerUseCase.Request?): Single<List<Question>> {
        return answerRepository.sendAnswer(params?: Request())
    }

    class Request(val protocolId: Int = 0,
                  val categoryId: Int = 0,
                  val questionId: Int = 0,
                  val id: Int = 0)
}