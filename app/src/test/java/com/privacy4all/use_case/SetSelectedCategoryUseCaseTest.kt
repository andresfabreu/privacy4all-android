package com.privacy4all.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.SetSelectedCategoryUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class SetSelectedCategoryUseCaseTest : UseCaseTest<SetSelectedCategoryUseCase, CategoryRepository, Unit>() {

    private val request = Category()

    override fun objectUnderTest(): SetSelectedCategoryUseCase {
        return SetSelectedCategoryUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): CategoryRepository {
        if (repository == null) {
            repository = Mockito.mock(CategoryRepository::class.java)
            Mockito.`when`(repository!!.setCategoryAsSelected(request)).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).setCategoryAsSelected(request)
    }

    override fun getSingle(): Single<Unit> {
        return objectUnderTest().getSingle(request)
    }

    override fun expectedValue() {
        return
    }

    @Test
    fun `when setSelectedCategoryUseCase is called, should call repository and return response`() {
        executeTest()
    }
}