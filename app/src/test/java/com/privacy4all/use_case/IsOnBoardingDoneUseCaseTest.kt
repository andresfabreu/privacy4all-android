package com.privacy4all.use_case

import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.use_case.IsOnBoardingDoneUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class IsOnBoardingDoneUseCaseTest : UseCaseTest<IsOnBoardingDoneUseCase, UserRepository, Boolean>() {
    override fun objectUnderTest(): IsOnBoardingDoneUseCase {
        return IsOnBoardingDoneUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): UserRepository {
        if (repository == null) {
            repository = Mockito.mock(UserRepository::class.java)
            Mockito.`when`(repository!!.isOnBoardingDone()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).isOnBoardingDone()
    }

    override fun getSingle(): Single<Boolean> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue(): Boolean {
        return true
    }

    @Test
    fun `when isOnBoardingDoneUseCase is called, should call repository and return response`() {
        executeTest()
    }
}