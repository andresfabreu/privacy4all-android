package com.privacy4all.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.GetSelectedCategoryUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class GetSelectedCategoryUseCaseTest : UseCaseTest<GetSelectedCategoryUseCase, CategoryRepository, Category>() {
    override fun objectUnderTest(): GetSelectedCategoryUseCase {
        return GetSelectedCategoryUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): CategoryRepository {
        if (repository == null) {
            repository = Mockito.mock(CategoryRepository::class.java)
            Mockito.`when`(repository!!.getSelectedCategory()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).getSelectedCategory()
    }

    override fun getSingle(): Single<Category> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue(): Category {
        return Category()
    }

    @Test
    fun `when getSelectedCategoryUseCase is called, should call repository and return response`() {
        executeTest()
    }
}