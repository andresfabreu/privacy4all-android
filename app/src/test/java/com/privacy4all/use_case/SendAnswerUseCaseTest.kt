package com.privacy4all.use_case

import com.privacy4all.data.repository.AnswerRepository
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.SendAnswerUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class SendAnswerUseCaseTest : UseCaseTest<SendAnswerUseCase, AnswerRepository, List<Question>>() {

    private val request = SendAnswerUseCase.Request()

    override fun objectUnderTest(): SendAnswerUseCase {
        return SendAnswerUseCase(refreshSessionUseCase, subscriberOn, observerOn, repository())
    }

    override fun repository(): AnswerRepository {
        if (repository == null) {
            repository = Mockito.mock(AnswerRepository::class.java)
            Mockito.`when`(repository!!.sendAnswer(request)).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).sendAnswer(request)
    }

    override fun getSingle(): Single<List<Question>> {
        return objectUnderTest().getSingle(request)
    }

    override fun expectedValue() : List<Question> {
        return listOf(Question())
    }

    @Test
    fun `when refreshSessionUseCase is called, should call repository and return response`() {
        executeTest()
    }
}