package com.privacy4all.use_case

import com.privacy4all.data.repository.AuthRepository
import com.privacy4all.domain.use_case.LoginWithFacebookUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class LoginWithFacebookUseCaseTest : UseCaseTest<LoginWithFacebookUseCase, AuthRepository, Unit>() {

    override fun objectUnderTest(): LoginWithFacebookUseCase {
        return LoginWithFacebookUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): AuthRepository {
        if (repository == null) {
            repository = Mockito.mock(AuthRepository::class.java)
            Mockito.`when`(repository!!.loginWithFacebook()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).loginWithFacebook()
    }

    override fun getSingle(): Single<Unit> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue() {
        return
    }

    @Test
    fun `when loginWithFacebookUseCase is called, should call repository and return response`() {
        executeTest()
    }
}