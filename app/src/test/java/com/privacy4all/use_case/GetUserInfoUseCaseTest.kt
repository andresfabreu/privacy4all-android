package com.privacy4all.use_case

import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.model.User
import com.privacy4all.domain.use_case.GetUserInfoUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class GetUserInfoUseCaseTest : UseCaseTest<GetUserInfoUseCase, UserRepository, User>() {
    override fun objectUnderTest(): GetUserInfoUseCase {
        return GetUserInfoUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): UserRepository {
        if (repository == null) {
            repository = Mockito.mock(UserRepository::class.java)
            Mockito.`when`(repository!!.getUserInfo()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).getUserInfo()
    }

    override fun getSingle(): Single<User> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue(): User {
        return User()
    }

    @Test
    fun `when getUserInfoUseCase is called, should call repository and return response`() {
        executeTest()
    }
}