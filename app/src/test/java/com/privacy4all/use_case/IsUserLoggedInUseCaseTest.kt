package com.privacy4all.use_case

import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.use_case.IsUserLoggedInUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class IsUserLoggedInUseCaseTest : UseCaseTest<IsUserLoggedInUseCase, UserRepository, Boolean>() {

    override fun objectUnderTest(): IsUserLoggedInUseCase {
        return IsUserLoggedInUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): UserRepository {
        if (repository == null) {
            repository = Mockito.mock(UserRepository::class.java)
            Mockito.`when`(repository!!.isUserLoggedIn()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).isUserLoggedIn()
    }

    override fun getSingle(): Single<Boolean> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue(): Boolean {
        return true
    }

    @Test
    fun `when isUserLoggedInUseCase is called, should call repository and return response`() {
        executeTest()
    }
}