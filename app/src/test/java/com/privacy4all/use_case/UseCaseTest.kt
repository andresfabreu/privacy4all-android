package com.privacy4all.use_case

import com.privacy4all.domain.executor.PostThreadExecutor
import com.privacy4all.domain.executor.ThreadExecutor
import com.privacy4all.domain.use_case.RefreshSessionUseCase
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

abstract class UseCaseTest<out UseCaseTest, RepositoryTest, ExpectedType> {

    @Mock
    lateinit var subscriberOn: ThreadExecutor

    @Mock
    lateinit var observerOn: PostThreadExecutor

    @Mock
    lateinit var refreshSessionUseCase: RefreshSessionUseCase

    var repository: RepositoryTest? = null

    abstract fun objectUnderTest(): UseCaseTest

    abstract fun repository(): RepositoryTest

    abstract fun repositoryMethodToCheck()

    abstract fun getSingle(): Single<ExpectedType>

    abstract fun expectedValue(): ExpectedType

    @Before
    @Suppress("UNCHECKED_CAST")
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(subscriberOn.scheduler)
                .thenReturn(Schedulers.io())

        Mockito.`when`(observerOn.scheduler)
                .thenReturn(Schedulers.io())
    }

    fun executeTest() {
        //given
        val testObserver = TestObserver<ExpectedType>()

        //when
        val result = getSingle()
        result.subscribe(testObserver)

        //then
        repositoryMethodToCheck()
        Mockito.verifyNoMoreInteractions(repository())

        testObserver.awaitDone(500, TimeUnit.MILLISECONDS)
        testObserver.assertComplete()
        testObserver.assertValue(expectedValue())
    }
}