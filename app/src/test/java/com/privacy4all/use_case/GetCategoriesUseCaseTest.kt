package com.privacy4all.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.GetCategoriesUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class GetCategoriesUseCaseTest : UseCaseTest<GetCategoriesUseCase, CategoryRepository, List<Category>>() {
    private val request = GetCategoriesUseCase.Request()

    override fun objectUnderTest(): GetCategoriesUseCase {
        return GetCategoriesUseCase(refreshSessionUseCase, subscriberOn, observerOn, repository())
    }

    override fun repository(): CategoryRepository {
        if (repository == null) {
            repository = Mockito.mock(CategoryRepository::class.java)
            Mockito.`when`(repository!!.getCategories(request)).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).getCategories(request)
    }

    override fun getSingle(): Single<List<Category>> {
        return objectUnderTest().getSingle(request)
    }

    override fun expectedValue(): List<Category> {
        return listOf(Category())
    }

    @Test
    fun `when getCategoriesUseCase is called, should call repository and return response`() {
        executeTest()
    }
}