package com.privacy4all.use_case

import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.use_case.SetOnBoardingAsDoneUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class SetOnBoardingAsDoneUseCaseTest : UseCaseTest<SetOnBoardingAsDoneUseCase, UserRepository, Unit>() {

    private val request = true

    override fun objectUnderTest(): SetOnBoardingAsDoneUseCase {
        return SetOnBoardingAsDoneUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): UserRepository {
        if (repository == null) {
            repository = Mockito.mock(UserRepository::class.java)
            Mockito.`when`(repository!!.setOnBoardingAsDone(request)).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).setOnBoardingAsDone(request)
    }

    override fun getSingle(): Single<Unit> {
        return objectUnderTest().getSingle(request)
    }

    override fun expectedValue() {
        return
    }

    @Test
    fun `when setOnBoardingAsDone is called, should call repository and return response`() {
        executeTest()
    }
}