package com.privacy4all.use_case

import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.use_case.ShouldReloadCategoriesUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class ShouldReloadCategoriesUseCaseTest : UseCaseTest<ShouldReloadCategoriesUseCase, CategoryRepository, Boolean>() {

    override fun objectUnderTest(): ShouldReloadCategoriesUseCase {
        return ShouldReloadCategoriesUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): CategoryRepository {
        if (repository == null) {
            repository = Mockito.mock(CategoryRepository::class.java)
            Mockito.`when`(repository!!.shouldReloadCategories()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).shouldReloadCategories()
    }

    override fun getSingle(): Single<Boolean> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue() : Boolean {
        return true
    }

    @Test
    fun `when shouldReloadCategoriesUseCase is called, should call repository and return response`() {
        executeTest()
    }
}