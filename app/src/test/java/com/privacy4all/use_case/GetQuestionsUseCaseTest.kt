package com.privacy4all.use_case

import com.privacy4all.common.CATEGORY_ID
import com.privacy4all.common.PROTOCOL_ID
import com.privacy4all.data.repository.QuestionRepository
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class GetQuestionsUseCaseTest : UseCaseTest<GetQuestionsUseCase, QuestionRepository, List<Question>>() {
    private val request = GetQuestionsUseCase.Request(PROTOCOL_ID, CATEGORY_ID)

    override fun objectUnderTest(): GetQuestionsUseCase {
        return GetQuestionsUseCase(refreshSessionUseCase, subscriberOn, observerOn, repository())
    }

    override fun repository(): QuestionRepository {
        if (repository == null) {
            repository = Mockito.mock(QuestionRepository::class.java)
            Mockito.`when`(repository!!.getQuestions(request)).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).getQuestions(request)
    }

    override fun getSingle(): Single<List<Question>> {
        return objectUnderTest().getSingle(request)
    }

    override fun expectedValue(): List<Question> {
        return listOf(Question())
    }

    @Test
    fun `when getQuestionsUseCase is called, should call repository and return response`() {
        executeTest()
    }
}