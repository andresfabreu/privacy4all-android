package com.privacy4all.use_case

import com.privacy4all.data.repository.AuthRepository
import com.privacy4all.domain.use_case.LogoutUseCase
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class LogoutUseCaseTest : UseCaseTest<LogoutUseCase, AuthRepository, Unit>() {

    override fun objectUnderTest(): LogoutUseCase {
        return LogoutUseCase(subscriberOn, observerOn, repository())
    }

    override fun repository(): AuthRepository {
        if (repository == null) {
            repository = Mockito.mock(AuthRepository::class.java)
            Mockito.`when`(repository!!.logout()).thenReturn(Single.just(expectedValue()))
        }

        return repository!!
    }

    override fun repositoryMethodToCheck() {
        Mockito.verify(repository(), Mockito.times(1)).logout()
    }

    override fun getSingle(): Single<Unit> {
        return objectUnderTest().getSingle()
    }

    override fun expectedValue() {
        return
    }

    @Test
    fun `when logoutUseCase is called, should call repository and return response`() {
        executeTest()
    }
}