package com.privacy4all.repository

import com.nhaarman.mockito_kotlin.any
import com.privacy4all.common.ACCESS_TOKEN
import com.privacy4all.common.USER
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import com.privacy4all.data.preferences.data_source.UserPreferencesDataSource
import com.privacy4all.data.preferences.mapper.UserPrefMapper
import com.privacy4all.data.preferences.model.UserPref
import com.privacy4all.data.repository.UserRepository
import com.privacy4all.domain.model.User
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class UserRepositoryTest {

    @Mock
    private lateinit var userPreferencesDataSource: UserPreferencesDataSource

    @Mock
    private lateinit var sessionPreferencesDataSource: SessionPreferencesDataSource

    @Mock
    private lateinit var userPrefMapper: UserPrefMapper

    private lateinit var objectUnderTest: UserRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(userPreferencesDataSource.putIsOnBoardingDone(any()))
                .thenReturn(Single.just(Unit))

        Mockito.`when`(userPreferencesDataSource.isOnBoardingDone())
                .thenReturn(Single.just(true))

        Mockito.`when`(userPreferencesDataSource.getUserPref())
                .thenReturn(Single.just(UserPref()))

        Mockito.`when`(sessionPreferencesDataSource.getSessionToken())
                .thenReturn(Single.just(ACCESS_TOKEN))

        Mockito.`when`(userPrefMapper.map(any<UserPref>()))
                .thenReturn(USER)

        objectUnderTest = UserRepository(userPreferencesDataSource, sessionPreferencesDataSource, userPrefMapper)
    }

    @Test
    fun `when setOnBoardingAsDone is request, should set into cache and return`() {
        //given
        val testObserver = TestObserver<Unit>()

        //when
        val result = objectUnderTest.setOnBoardingAsDone(true)
        result.subscribe(testObserver)

        //then
        Mockito.verify(userPreferencesDataSource, Mockito.times(1)).putIsOnBoardingDone(true)
        Mockito.verifyNoMoreInteractions(userPreferencesDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(Unit)
    }

    @Test
    fun `when isOnBoardingDone is request, should return the value from cache`() {
        //given
        val testObserver = TestObserver<Boolean>()

        //when
        val result = objectUnderTest.isOnBoardingDone()
        result.subscribe(testObserver)

        //then
        Mockito.verify(userPreferencesDataSource, Mockito.times(1)).isOnBoardingDone()
        Mockito.verifyNoMoreInteractions(userPreferencesDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(true)
    }

    @Test
    fun `when isUserLoggedIn is request, should return if access token != "" from cache`() {
        //given
        val testObserver = TestObserver<Boolean>()

        //when
        val result = objectUnderTest.isUserLoggedIn()
        result.subscribe(testObserver)

        //then
        Mockito.verify(sessionPreferencesDataSource, Mockito.times(1)).getSessionToken()
        Mockito.verifyNoMoreInteractions(sessionPreferencesDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(true)
    }

    @Test
    fun `when getUserInfo is request, should return the user from cache`() {
        //given
        val testObserver = TestObserver<User>()

        //when
        val result = objectUnderTest.getUserInfo()
        result.subscribe(testObserver)

        //then
        Mockito.verify(userPreferencesDataSource, Mockito.times(1)).getUserPref()
        Mockito.verifyNoMoreInteractions(userPreferencesDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(USER)
    }
}