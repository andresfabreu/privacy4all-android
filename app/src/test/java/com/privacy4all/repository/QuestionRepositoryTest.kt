package com.privacy4all.repository

import com.nhaarman.mockito_kotlin.any
import com.privacy4all.common.CATEGORY_ID
import com.privacy4all.common.PROTOCOL_ID
import com.privacy4all.common.QUESTION_API_RESPONSE
import com.privacy4all.common.QUESTION_LIST
import com.privacy4all.data.network.data_source.QuestionApiDataSource
import com.privacy4all.data.network.mapper.QuestionApiMapper
import com.privacy4all.data.network.model.QuestionApi
import com.privacy4all.data.network.model.QuestionApiRequest
import com.privacy4all.data.repository.QuestionRepository
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.GetQuestionsUseCase
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class QuestionRepositoryTest {

    @Mock
    private lateinit var questionApiDataSource: QuestionApiDataSource

    @Mock
    private lateinit var questionApiMapper: QuestionApiMapper

    private lateinit var objectUnderTest: QuestionRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(questionApiDataSource.getQuestions(any(), any()))
                .thenReturn(Single.just(QUESTION_API_RESPONSE))

        Mockito.`when`(questionApiMapper.map(any<List<QuestionApi>>()))
                .thenReturn(QUESTION_LIST)

        Mockito.`when`(questionApiMapper.map(any<GetQuestionsUseCase.Request>()))
                .thenReturn(QuestionApiRequest(PROTOCOL_ID, CATEGORY_ID))


        objectUnderTest = QuestionRepository(questionApiDataSource, questionApiMapper)
    }

    @Test
    fun `when questions are request, should call api and return data`() {
        //given
        val testObserver = TestObserver<List<Question>>()

        //when
        val result = objectUnderTest.getQuestions(GetQuestionsUseCase.Request(PROTOCOL_ID, CATEGORY_ID))
        result.subscribe(testObserver)

        //then
        Mockito.verify(questionApiDataSource, Mockito.times(1)).getQuestions(any(), any())
        Mockito.verifyNoMoreInteractions(questionApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(QUESTION_LIST)
    }
}