package com.privacy4all.repository

import android.content.Context
import com.nhaarman.mockito_kotlin.any
import com.privacy4all.common.DEVICE_ID
import com.privacy4all.common.LOGIN_API_RESPONSE
import com.privacy4all.common.USER_PREF
import com.privacy4all.data.network.data_source.AuthApiDataSource
import com.privacy4all.data.network.model.LoginApiRequest
import com.privacy4all.data.network.model.LoginApiResponse
import com.privacy4all.data.preferences.data_source.DevicePreferencesDataSource
import com.privacy4all.data.preferences.data_source.SessionPreferencesDataSource
import com.privacy4all.data.preferences.data_source.UserPreferencesDataSource
import com.privacy4all.data.preferences.mapper.UserPrefMapper
import com.privacy4all.data.repository.AuthRepository
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.adapter.rxjava2.Result

class AuthRepositoryTest {

    @Mock
    private lateinit var authApiDataSource: AuthApiDataSource

    @Mock
    private lateinit var sessionPreferencesDataSource: SessionPreferencesDataSource

    @Mock
    private lateinit var userPreferencesDataSource: UserPreferencesDataSource

    @Mock
    private lateinit var devicePreferencesDataSource: DevicePreferencesDataSource

    @Mock
    private lateinit var userPrefMapper: UserPrefMapper

    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var unitResult: Result<Unit>

    private lateinit var objectUnderTest: AuthRepository

    private fun AuthRepository.loginWithFacebookTest(): Single<Unit> {
        // Removed the facebook sdk part for test

        return authApiDataSource.loginWithFacebook(LoginApiRequest(DEVICE_ID))
                .flatMap { resp -> userPreferencesDataSource.putUserPref(userPrefMapper.map(resp)).map { resp } }
                .flatMap { resp -> sessionPreferencesDataSource.putSessionToken(resp.accessToken) }
                .map { Unit }
    }

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(authApiDataSource.logout())
                .thenReturn(Single.just(unitResult))

        Mockito.`when`(authApiDataSource.loginWithUuid(any()))
                .thenReturn(Single.just(LOGIN_API_RESPONSE))

        Mockito.`when`(authApiDataSource.loginWithFacebook(any()))
                .thenReturn(Single.just(LOGIN_API_RESPONSE))

        Mockito.`when`(sessionPreferencesDataSource.clearSessionPreferences())
                .thenReturn(Single.just(Unit))

        Mockito.`when`(sessionPreferencesDataSource.putSessionToken(any()))
                .thenReturn(Single.just(Unit))

        Mockito.`when`(userPreferencesDataSource.clearUserPreferences())
                .thenReturn(Single.just(Unit))

        Mockito.`when`(userPreferencesDataSource.putUserPref(any()))
                .thenReturn(Single.just(Unit))

        Mockito.`when`(devicePreferencesDataSource.clearDevicePreferences())
                .thenReturn(Single.just(Unit))

        Mockito.`when`(devicePreferencesDataSource.getDeviceId())
                .thenReturn(Single.just(DEVICE_ID))

        Mockito.`when`(devicePreferencesDataSource.putDeviceId(any()))
                .thenReturn(Single.just(DEVICE_ID))

        Mockito.`when`(userPrefMapper.map(any<LoginApiResponse>()))
                .thenReturn(USER_PREF)

        objectUnderTest = AuthRepository(authApiDataSource,
                sessionPreferencesDataSource,
                userPreferencesDataSource,
                devicePreferencesDataSource,
                userPrefMapper,
                context)
    }

    @Test
    fun `when logout is request, should call api and return`() {
        //given
        val testObserver = TestObserver<Unit>()

        //when
        val result = objectUnderTest.logout()
        result.subscribe(testObserver)

        //then
        Mockito.verify(authApiDataSource, Mockito.times(1)).logout()
        Mockito.verifyNoMoreInteractions(authApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(Unit)
    }

    @Test
    fun `when loginWithUUID is request, should call api and return`() {
        //given
        val testObserver = TestObserver<Unit>()

        //when
        val result = objectUnderTest.loginWithUuid(DEVICE_ID)
        result.subscribe(testObserver)

        //then
        Mockito.verify(authApiDataSource, Mockito.times(1)).loginWithUuid(LoginApiRequest(DEVICE_ID))
        Mockito.verifyNoMoreInteractions(authApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(Unit)
    }

    @Test
    fun `when loginWithFacebook is request, should call api and then return`() {
        //given
        val testObserver = TestObserver<Unit>()

        //when
        val result = objectUnderTest.loginWithFacebookTest()
        result.subscribe(testObserver)

        //then
        Mockito.verify(authApiDataSource, Mockito.times(1)).loginWithFacebook(LoginApiRequest(DEVICE_ID))
        Mockito.verifyNoMoreInteractions(authApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(Unit)
    }

    @Test
    fun `when refresh session is request, should call loginWithUUID and then return`() {
        //given
        val testObserver = TestObserver<Unit>()

        //when
        val result = objectUnderTest.refreshSession(DEVICE_ID)
        result.subscribe(testObserver)

        //then
        Mockito.verify(authApiDataSource, Mockito.times(1)).loginWithUuid(LoginApiRequest(DEVICE_ID))
        Mockito.verifyNoMoreInteractions(authApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(Unit)
    }
}