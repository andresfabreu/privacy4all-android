package com.privacy4all.repository

import com.nhaarman.mockito_kotlin.any
import com.privacy4all.common.CATEGORY_API_RESPONSE
import com.privacy4all.common.PROTOCOL_ID
import com.privacy4all.data.in_memory.data_source.CategoryInMemoryDataSource
import com.privacy4all.data.in_memory.mapper.CategoryInMemoryMapper
import com.privacy4all.data.in_memory.model.CategoryInMemory
import com.privacy4all.data.network.data_source.CategoryApiDataSource
import com.privacy4all.data.network.mapper.CategoryApiMapper
import com.privacy4all.data.network.model.CategoryApi
import com.privacy4all.data.repository.CategoryRepository
import com.privacy4all.domain.model.Category
import com.privacy4all.domain.use_case.GetCategoriesUseCase
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CategoryRepositoryTest {

    @Mock
    private lateinit var categoryApiDataSource: CategoryApiDataSource

    @Mock
    private lateinit var categoryInMemoryDataSource: CategoryInMemoryDataSource

    @Mock
    private lateinit var categoryApiMapper: CategoryApiMapper

    @Mock
    private lateinit var categoryInMemoryMapper: CategoryInMemoryMapper

    private lateinit var objectUnderTest: CategoryRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(categoryApiDataSource.getCategories(any()))
                .thenReturn(Single.just(CATEGORY_API_RESPONSE))

        Mockito.`when`(categoryApiMapper.map(any<List<CategoryApi>>()))
                .thenReturn(listOf(Category()))

        Mockito.`when`(categoryInMemoryDataSource.removeShouldReloadCategories())
                .thenReturn(Single.just(Unit))

        Mockito.`when`(categoryInMemoryDataSource.shouldReloadCategories())
                .thenReturn(Single.just(true))

        Mockito.`when`(categoryInMemoryDataSource.putCategory(any()))
                .thenReturn(Single.just(Unit))

        Mockito.`when`(categoryInMemoryDataSource.getCategory())
                .thenReturn(Single.just(CategoryInMemory()))

        Mockito.`when`(categoryInMemoryDataSource.removeCategory())
                .thenReturn(Single.just(Unit))

        Mockito.`when`(categoryInMemoryMapper.map(any<Category>()))
                .thenReturn(CategoryInMemory())

        Mockito.`when`(categoryInMemoryMapper.map(any<CategoryInMemory>()))
                .thenReturn(Category())

        objectUnderTest = CategoryRepository(categoryApiDataSource,
                categoryInMemoryDataSource,
                categoryApiMapper,
                categoryInMemoryMapper)
    }

    @Test
    fun `when categories are request, should call api and return data`() {
        //given
        val testObserver = TestObserver<List<Category>>()

        //when
        val result = objectUnderTest.getCategories(GetCategoriesUseCase.Request())
        result.subscribe(testObserver)

        //then
        Mockito.verify(categoryApiDataSource, Mockito.times(1)).getCategories(PROTOCOL_ID)
        Mockito.verifyNoMoreInteractions(categoryApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(listOf(Category()))
    }

    @Test
    fun `when setCategoryAsSelected are request, should set into cache and return`() {
        //given
        val testObserver = TestObserver<Unit>()

        //when
        val result = objectUnderTest.setCategoryAsSelected(Category())
        result.subscribe(testObserver)

        //then
        Mockito.verify(categoryInMemoryDataSource, Mockito.times(1)).putCategory(CategoryInMemory())
        Mockito.verifyNoMoreInteractions(categoryInMemoryDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(Unit)
    }

    @Test
    fun `when getSelectedCategory are request, should return the value from cache`() {
        //given
        val testObserver = TestObserver<Category>()

        //when
        val result = objectUnderTest.getSelectedCategory()
        result.subscribe(testObserver)

        //then
        testObserver.assertComplete()
        testObserver.assertValue(Category())
    }

    @Test
    fun `when shouldReloadCategories are request, should return the value from cache`() {
        //given
        val testObserver = TestObserver<Boolean>()

        //when
        val result = objectUnderTest.shouldReloadCategories()
        result.subscribe(testObserver)

        //then
        testObserver.assertComplete()
        testObserver.assertValue(true)
    }
}