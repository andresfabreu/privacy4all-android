package com.privacy4all.repository

import com.nhaarman.mockito_kotlin.any
import com.privacy4all.common.ANSWER_API_REQUEST
import com.privacy4all.common.QUESTION_API_RESPONSE
import com.privacy4all.common.QUESTION_LIST
import com.privacy4all.data.in_memory.data_source.CategoryInMemoryDataSource
import com.privacy4all.data.network.data_source.AnswerApiDataSource
import com.privacy4all.data.network.mapper.AnswerApiMapper
import com.privacy4all.data.network.mapper.QuestionApiMapper
import com.privacy4all.data.network.model.QuestionApi
import com.privacy4all.data.repository.AnswerRepository
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.use_case.SendAnswerUseCase
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class AnswerRepositoryTest {

    @Mock
    private lateinit var answerApiDataSource: AnswerApiDataSource

    @Mock
    private lateinit var categoryInMemoryDataSource: CategoryInMemoryDataSource

    @Mock
    private lateinit var questionApiMapper: QuestionApiMapper

    @Mock
    private lateinit var answerApiMapper: AnswerApiMapper

    private lateinit var objectUnderTest: AnswerRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(answerApiDataSource.senAnswer(any()))
                .thenReturn(Single.just(QUESTION_API_RESPONSE))

        Mockito.`when`(questionApiMapper.map(any<List<QuestionApi>>()))
                .thenReturn(QUESTION_LIST)

        Mockito.`when`(answerApiMapper.map(any<SendAnswerUseCase.Request>()))
                .thenReturn(ANSWER_API_REQUEST)

        Mockito.`when`(categoryInMemoryDataSource.putShouldReloadCategories(any()))
                .thenReturn(Single.just(Unit))

        objectUnderTest = AnswerRepository(answerApiDataSource, categoryInMemoryDataSource, questionApiMapper, answerApiMapper)
    }

    @Test
    fun `when an answer is send, should call api and return data`() {
        //given
        val testObserver = TestObserver<List<Question>>()

        //when
        val result = objectUnderTest.sendAnswer(SendAnswerUseCase.Request())
        result.subscribe(testObserver)

        //then
        Mockito.verify(answerApiDataSource, Mockito.times(1)).senAnswer(ANSWER_API_REQUEST)
        Mockito.verifyNoMoreInteractions(answerApiDataSource)

        testObserver.assertComplete()
        testObserver.assertValue(QUESTION_LIST)
    }
}