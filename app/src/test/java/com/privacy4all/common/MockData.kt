package com.privacy4all.common

import com.privacy4all.data.network.model.*
import com.privacy4all.data.preferences.model.UserPref
import com.privacy4all.domain.model.Question
import com.privacy4all.domain.model.User

const val DEVICE_ID = "JNmnvmdsmvJSFSFHjksdn12387rSF"
const val NAME = "a9981d3751cb0b0f5c3eebe2c20d9035@uuid.com"
const val ACCESS_TOKEN = "msdddce4q8t9mtkoeomdh36krutjquoo"
const val PROTOCOL_ID = 1
const val CATEGORY_ID = 1
const val USER_ID = 1L

val LOGIN_API_RESPONSE = LoginApiResponse(USER_ID,
        NAME,
        "",
        "",
        ACCESS_TOKEN)

val USER_PREF = UserPref(NAME,
        "")

val USER = User(NAME,
        "")

val CATEGORY_API_RESPONSE = CategoryApiResponse()

val QUESTION_API_LIST = listOf(QuestionApi())
val QUESTION_API_RESPONSE = QuestionApiResponse(QUESTION_API_LIST)
val QUESTION_LIST = listOf(Question())

val ANSWER_API_REQUEST = AnswerApiRequest()